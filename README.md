# README #

### What is this repository for? ###

Internal Sokowatch application for ETL of data from QB into warehouse
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###
In order to run and test this app locally, you need a http tunneling tool to be running such as ngrok because QB enforces a non-local https
connection in the redirectUri for Oauth authentication. This is specifically for the "Connect to Quickbooks" button on the home page.
When clicking on this button the user is authenticated against QB via Oauth before they can associate their QB entity with this app (only 
really needs to happen once per entity).

This gets setup here https://developer.intuit.com/app/developer/ under the Production
Keys & Oauth section. Once the tunneler has given you your https Uri, you need to add/replace it in the redirect Uri's for the app.
(with /oauth2redirect) appended to the end of the Uri. Please note, you also then need to update the QBOOAuth2AppRedirectUri string in the
QBOOAuth2AppRedirectUri.txt file in the config folder

You then need to do the same for the Oauth Google authentication at https://console.developers.google.com/apis/ where you update the <b>credentials</b>
for the app under the Authorized redirect URIs. Use the following format http://<tunnel Uri>/login/oauth2/code/google. Please note, there is no http<b>s</b>.
It must be http (not sure why). I think there is a setting in the security.config.SecurityConfig class, but I couldn't get it working for some reason.
So I have kep it as http. You don't need to update anything in the config files for this

In a nutshell, the steps when trying to run it locally are:
- Ensure all the config files are correct in the config folder
- Ensure the application.config file is correct
- Create tunnel and take note of the new Uri
- update QBOOAuth2AppRedirectUri string in QBOOAuth2AppRedirectUri.txt config file with new Uri
- Update Uri in QB developer console for new redirect Uri
- Update Google developer console redirect Uri with new Uri
- Start app and navigate to new Uri

When deploying to live, change the ConfigFileLocation property in application.config to the correct location on the server "/opt/tomcat/SokowatchAccountingInterface/config/"
Then do a maven install command and copy the war file created to /home/soko/ROOT.war on the server
Then move the war file to /opt/tomcat/webapps/
The config files for the app are in "/opt/tomcat/SokowatchAccountingInterface/config/". You shouldn't need to change these files.

If Sokowatch opens a new entity, you will need to add a file into the config folder in the same format as the other Company.StartDate.[RealmId].txt files to represent the start date of the new entity (ie the date of the first accounting entry).
You get the RealmId of the new entity by going to QB online interface, open the company and press CTRL+ALT+? and a window pops up with a Company ID.  Hit the copy button.

Once you have set up the company start date, you need to add a button for the new entity to the connected.html page. You can copy one of the other buttons and just change the RealmId.

You then need to enable this app to use the new entity by clicking on the Connect to Quickbooks button on the home page of this app. Once that is succesfully completed, you will be able to use the new entity.



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
Keith Davies. davies.keith@gmail.com