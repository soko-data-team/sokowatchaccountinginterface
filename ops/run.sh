#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  #./gradlew bootRun
  #./gradlew clean bootRun
  #./gradlew bootRun -Dspring-boot.run.profiles=dev -Dmaven.test.skip=true -Djacoco.skip=true
  #java -jar build/libs/*-SNAPSHOT.jar
  mvn clean spring-boot:run

popd
