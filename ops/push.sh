#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )
pushd $DIR/..
  git add .
  git commit -m "Autopush"
  git push -f origin master
popd
