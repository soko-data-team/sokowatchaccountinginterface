--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

-- Started on 2022-04-26 10:14:32

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 211 (class 1259 OID 24838)
-- Name: completed_efris_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.completed_efris_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    order_signature character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    call_back_response character varying NOT NULL,
    response_time timestamp without time zone NOT NULL
);


ALTER TABLE public.completed_efris_signatures OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 24836)
-- Name: completed_efris_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.completed_efris_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.completed_efris_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2931 (class 0 OID 0)
-- Dependencies: 210
-- Name: completed_efris_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.completed_efris_signatures_id_seq OWNED BY public.completed_efris_signatures.id;


--
-- TOC entry 207 (class 1259 OID 24807)
-- Name: completed_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.completed_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    order_signature character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    call_back_response character varying NOT NULL,
    response_time timestamp without time zone NOT NULL
)
WITH (autovacuum_enabled='true', autovacuum_vacuum_threshold='500', autovacuum_analyze_threshold='500', autovacuum_vacuum_scale_factor='0', autovacuum_analyze_scale_factor='0');


ALTER TABLE public.completed_signatures OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 24805)
-- Name: completed_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.completed_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.completed_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2932 (class 0 OID 0)
-- Dependencies: 206
-- Name: completed_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.completed_signatures_id_seq OWNED BY public.completed_signatures.id;


--
-- TOC entry 217 (class 1259 OID 152076)
-- Name: completed_tra_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.completed_tra_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    order_signature character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    call_back_response character varying NOT NULL,
    response_time timestamp without time zone NOT NULL
);


ALTER TABLE public.completed_tra_signatures OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 152074)
-- Name: completed_tra_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.completed_tra_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.completed_tra_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2933 (class 0 OID 0)
-- Dependencies: 216
-- Name: completed_tra_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.completed_tra_signatures_id_seq OWNED BY public.completed_tra_signatures.id;


--
-- TOC entry 223 (class 1259 OID 176686)
-- Name: completed_zreports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.completed_zreports (
    id integer NOT NULL,
    report_id character varying NOT NULL,
    report_json character varying NOT NULL,
    zreport_number character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    call_back_response character varying NOT NULL,
    response_time timestamp without time zone NOT NULL
);


ALTER TABLE public.completed_zreports OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 176684)
-- Name: completed_zreports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.completed_zreports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.completed_zreports_id_seq OWNER TO postgres;

--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 222
-- Name: completed_zreports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.completed_zreports_id_seq OWNED BY public.completed_zreports.id;


--
-- TOC entry 203 (class 1259 OID 24785)
-- Name: config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.config (
    id integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
)
WITH (autovacuum_enabled='true', autovacuum_vacuum_threshold='500', autovacuum_analyze_threshold='500', autovacuum_vacuum_scale_factor='0', autovacuum_analyze_scale_factor='0');


ALTER TABLE public.config OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24783)
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.config_id_seq OWNER TO postgres;

--
-- TOC entry 2935 (class 0 OID 0)
-- Dependencies: 202
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.config_id_seq OWNED BY public.config.id;


--
-- TOC entry 213 (class 1259 OID 24849)
-- Name: efris_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.efris_config (
    id integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
);


ALTER TABLE public.efris_config OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 24847)
-- Name: efris_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.efris_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.efris_config_id_seq OWNER TO postgres;

--
-- TOC entry 2936 (class 0 OID 0)
-- Dependencies: 212
-- Name: efris_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.efris_config_id_seq OWNED BY public.efris_config.id;


--
-- TOC entry 209 (class 1259 OID 24827)
-- Name: pending_efris_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pending_efris_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    is_credit_note boolean
);


ALTER TABLE public.pending_efris_signatures OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 24825)
-- Name: pending_efris_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pending_efris_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pending_efris_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 208
-- Name: pending_efris_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pending_efris_signatures_id_seq OWNED BY public.pending_efris_signatures.id;


--
-- TOC entry 205 (class 1259 OID 24796)
-- Name: pending_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pending_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL
)
WITH (autovacuum_enabled='true', autovacuum_vacuum_threshold='500', autovacuum_analyze_threshold='500', autovacuum_vacuum_scale_factor='0', autovacuum_analyze_scale_factor='0');


ALTER TABLE public.pending_signatures OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 24794)
-- Name: pending_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pending_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pending_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 204
-- Name: pending_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pending_signatures_id_seq OWNED BY public.pending_signatures.id;


--
-- TOC entry 215 (class 1259 OID 152065)
-- Name: pending_tra_signatures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pending_tra_signatures (
    id integer NOT NULL,
    order_id character varying NOT NULL,
    order_json character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL,
    is_credit_note boolean,
    ims_call_back_response character varying
);


ALTER TABLE public.pending_tra_signatures OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 152063)
-- Name: pending_tra_signatures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pending_tra_signatures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pending_tra_signatures_id_seq OWNER TO postgres;

--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 214
-- Name: pending_tra_signatures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pending_tra_signatures_id_seq OWNED BY public.pending_tra_signatures.id;


--
-- TOC entry 221 (class 1259 OID 176664)
-- Name: pending_zreports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pending_zreports (
    id integer NOT NULL,
    report_id character varying NOT NULL,
    report_json character varying NOT NULL,
    call_back_name character varying NOT NULL,
    request_time timestamp without time zone NOT NULL
);


ALTER TABLE public.pending_zreports OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 176662)
-- Name: pending_zreports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pending_zreports_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pending_zreports_id_seq OWNER TO postgres;

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 220
-- Name: pending_zreports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pending_zreports_id_seq OWNED BY public.pending_zreports.id;


--
-- TOC entry 219 (class 1259 OID 152088)
-- Name: tra_config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tra_config (
    id integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
);


ALTER TABLE public.tra_config OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 152086)
-- Name: tra_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tra_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tra_config_id_seq OWNER TO postgres;

--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 218
-- Name: tra_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tra_config_id_seq OWNED BY public.tra_config.id;


--
-- TOC entry 224 (class 1259 OID 176870)
-- Name: tra_config_prod; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tra_config_prod (
    id integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
);


ALTER TABLE public.tra_config_prod OWNER TO postgres;

--
-- TOC entry 2767 (class 2604 OID 24841)
-- Name: completed_efris_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_efris_signatures ALTER COLUMN id SET DEFAULT nextval('public.completed_efris_signatures_id_seq'::regclass);


--
-- TOC entry 2765 (class 2604 OID 24810)
-- Name: completed_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_signatures ALTER COLUMN id SET DEFAULT nextval('public.completed_signatures_id_seq'::regclass);


--
-- TOC entry 2770 (class 2604 OID 152079)
-- Name: completed_tra_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_tra_signatures ALTER COLUMN id SET DEFAULT nextval('public.completed_tra_signatures_id_seq'::regclass);


--
-- TOC entry 2773 (class 2604 OID 176689)
-- Name: completed_zreports id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_zreports ALTER COLUMN id SET DEFAULT nextval('public.completed_zreports_id_seq'::regclass);


--
-- TOC entry 2763 (class 2604 OID 24788)
-- Name: config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config ALTER COLUMN id SET DEFAULT nextval('public.config_id_seq'::regclass);


--
-- TOC entry 2768 (class 2604 OID 24852)
-- Name: efris_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.efris_config ALTER COLUMN id SET DEFAULT nextval('public.efris_config_id_seq'::regclass);


--
-- TOC entry 2766 (class 2604 OID 24830)
-- Name: pending_efris_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_efris_signatures ALTER COLUMN id SET DEFAULT nextval('public.pending_efris_signatures_id_seq'::regclass);


--
-- TOC entry 2764 (class 2604 OID 24799)
-- Name: pending_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_signatures ALTER COLUMN id SET DEFAULT nextval('public.pending_signatures_id_seq'::regclass);


--
-- TOC entry 2769 (class 2604 OID 152068)
-- Name: pending_tra_signatures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_tra_signatures ALTER COLUMN id SET DEFAULT nextval('public.pending_tra_signatures_id_seq'::regclass);


--
-- TOC entry 2772 (class 2604 OID 176667)
-- Name: pending_zreports id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_zreports ALTER COLUMN id SET DEFAULT nextval('public.pending_zreports_id_seq'::regclass);


--
-- TOC entry 2771 (class 2604 OID 152091)
-- Name: tra_config id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tra_config ALTER COLUMN id SET DEFAULT nextval('public.tra_config_id_seq'::regclass);


--
-- TOC entry 2785 (class 2606 OID 24859)
-- Name: efris_config efris_config_name_value_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.efris_config
    ADD CONSTRAINT efris_config_name_value_key UNIQUE (name, value);


--
-- TOC entry 2783 (class 2606 OID 24846)
-- Name: completed_efris_signatures pk_completed_efris_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_efris_signatures
    ADD CONSTRAINT pk_completed_efris_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2779 (class 2606 OID 24815)
-- Name: completed_signatures pk_completed_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_signatures
    ADD CONSTRAINT pk_completed_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2791 (class 2606 OID 152084)
-- Name: completed_tra_signatures pk_completed_tra_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_tra_signatures
    ADD CONSTRAINT pk_completed_tra_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2797 (class 2606 OID 176694)
-- Name: completed_zreports pk_completed_zreport_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.completed_zreports
    ADD CONSTRAINT pk_completed_zreport_id PRIMARY KEY (id);


--
-- TOC entry 2775 (class 2606 OID 24793)
-- Name: config pk_config_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT pk_config_id PRIMARY KEY (id);


--
-- TOC entry 2787 (class 2606 OID 24857)
-- Name: efris_config pk_efris_config_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.efris_config
    ADD CONSTRAINT pk_efris_config_id PRIMARY KEY (id);


--
-- TOC entry 2781 (class 2606 OID 24835)
-- Name: pending_efris_signatures pk_pending_efris_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_efris_signatures
    ADD CONSTRAINT pk_pending_efris_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2777 (class 2606 OID 24804)
-- Name: pending_signatures pk_pending_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_signatures
    ADD CONSTRAINT pk_pending_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2789 (class 2606 OID 152073)
-- Name: pending_tra_signatures pk_pending_tra_signatures_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_tra_signatures
    ADD CONSTRAINT pk_pending_tra_signatures_id PRIMARY KEY (id);


--
-- TOC entry 2795 (class 2606 OID 176672)
-- Name: pending_zreports pk_pending_zreport_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pending_zreports
    ADD CONSTRAINT pk_pending_zreport_id PRIMARY KEY (id);


--
-- TOC entry 2793 (class 2606 OID 152096)
-- Name: tra_config pk_tra_config_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tra_config
    ADD CONSTRAINT pk_tra_config_id PRIMARY KEY (id);


--
-- TOC entry 2799 (class 2606 OID 176877)
-- Name: tra_config_prod pk_tra_prod_config_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tra_config_prod
    ADD CONSTRAINT pk_tra_prod_config_id PRIMARY KEY (id);


-- Completed on 2022-04-26 10:14:32

--
-- PostgreSQL database dump complete
--

