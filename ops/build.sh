#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  #gradle build
  mvn clean install -Dmaven.test.skip=true -Djacoco.skip=true
  #./gradlew clean build -x test -Djacoco.skip=true
  mkdir -p build/libs
  cp target/*SNAPSHOT.jar build/libs/app-SNAPSHOT.jar
popd


