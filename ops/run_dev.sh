#!/bin/bash -e

DIR=$( cd "$( dirname "$0" )" && pwd )

pushd $DIR/..
  #./gradlew bootRun
  #./gradlew clean bootRun
  mvn clean spring-boot:run

  #./gradlew bootRun -Dspring-boot.run.profiles=dev -Dmaven.test.skip=true -Djacoco.skip=true
  #java -jar build/libs/*-SNAPSHOT.jar
popd
