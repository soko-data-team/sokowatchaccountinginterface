Output From Date is the date from which the data gets outputed to the CSV file/sent to BigQuery.  Note, this does not impact the from date where the
data is fetched from QB as that needs to go back to the start in order to calculate closing balances correctly
this therefore just impacts what goes to the CSV file/BigQuery
Use format yyyy-mm-dd