package com.sokowatch.accountinginterface.dto;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Currency;

import org.junit.jupiter.api.Test;

import com.sokowatch.accountinginterface.enums.DataVersion;
import com.sokowatch.accountinginterface.model.AccountClassLocation;
import com.sokowatch.accountinginterface.model.AccountingPackageAccount;
import com.sokowatch.accountinginterface.model.LedgerEntry;

public class SummaryGLLineDTOTest {
	
	@Test
	public void testObjectToDTO() {
		LocalDateTime nowDateTime = LocalDateTime.now();
		
		SummaryGLLineDTO dto1 = new SummaryGLLineDTO(DataVersion.ACTUAL, nowDateTime, "Company Name", 
				"12345", 
				"Name 1", 
				true,
				Currency.getInstance("KES"), 
				Currency.getInstance("KES"),
				"Classification 1",
				LocalDate.of(2020, 5, 31), 
				"Dummy AC", 
				"Dummy AL",
				new BigDecimal(100.23), 
				new BigDecimal(-86.7));
		
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum(dto1.getAccountNum());
		accountingPackageAccount.setCurrency(dto1.getAccountCurrency());
		accountingPackageAccount.setCurrency(dto1.getCompanyCurrency());
		accountingPackageAccount.setIsCurrencyValue(true);
		accountingPackageAccount.setName(dto1.getAccountName());
		accountingPackageAccount.setClassification(dto1.getAccountClassification());
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setCompanyName(dto1.getCompanyName());
		accountClassLocation.setCompanyCurrency(dto1.getCompanyCurrency());
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag(dto1.getAccountingClass());
		accountClassLocation.setAccountingLocation(dto1.getAccountingLocation());
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setDate(LocalDate.of(2020, 4, 10)); //month before
		ledgerEntry1.setAmount(dto1.getAmount().multiply(new BigDecimal(-1)).add(dto1.getClosingBalance()));
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setDate(LocalDate.of(2020, 5, 10)); 
		ledgerEntry2.setAmount(new BigDecimal(100));		
		
		LedgerEntry ledgerEntry3 = new LedgerEntry();
		ledgerEntry3.setDate(LocalDate.of(2020, 5, 28)); 
		ledgerEntry3.setAmount(dto1.getAmount().subtract(ledgerEntry2.getAmount()));
		
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		ledgerEntry2.setAccountClassLocation(accountClassLocation);
		ledgerEntry3.setAccountClassLocation(accountClassLocation);
		
		assertThat(dto1.equals(ObjectToDtoFacade.convertToDto(DataVersion.ACTUAL, nowDateTime, LocalDate.of(2020, 5, 2), accountClassLocation)), is(true)); //I am specifically passing a "non-end-of-month" date because the method should convert it to the last day of the month itself
		assertThat(dto1.hashCode() == ObjectToDtoFacade.convertToDto(DataVersion.ACTUAL, nowDateTime, LocalDate.of(2020, 5, 31), accountClassLocation).hashCode(), is(true)); //I am specifically passing a "non-end-of-month" date because the method should convert it to the last day of the month itself		
	}

}
