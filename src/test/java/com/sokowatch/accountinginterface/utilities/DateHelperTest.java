package com.sokowatch.accountinginterface.utilities;

import org.junit.jupiter.api.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import java.time.LocalDate;

public class DateHelperTest {
	@Test
	public void testConstrainedDate() {
		LocalDate calculatedDate = LocalDate.of(2020, 10, 1);
		LocalDate maxDate = LocalDate.of(2020, 10, 25);
		
		assertThat(DateHelper.getConstrainedDate(calculatedDate, maxDate), is(calculatedDate));
		calculatedDate = LocalDate.of(2020, 10, 26);
		assertThat(DateHelper.getConstrainedDate(calculatedDate, maxDate), is(maxDate));
		calculatedDate = LocalDate.of(2020, 10, 25);
		assertThat(DateHelper.getConstrainedDate(calculatedDate, maxDate), is(maxDate));
		
	}

}
