package com.sokowatch.accountinginterface.helper;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.sokowatch.accountinginterface.Application;
import com.sokowatch.accountinginterface.encrypt.FileEncrypterDecrypter;
import com.sokowatch.accountinginterface.encrypt.SecretKeyHelper;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
public class EncryptionHelperTest {
	@Autowired
	SecretKeyHelper secretKeyHelper;
	
	@Test
	public void whenEncryptingIntoFile_andDecryptingFileAgain_thenOriginalStringIsReturned() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException, InvalidAlgorithmParameterException {
	    String originalContent = "foobar";
	    
	    SecretKey secretKey;
	    if(! secretKeyHelper.keyExists()) {
	    	secretKey = secretKeyHelper.generateKey();
	    	secretKeyHelper.saveKey(secretKey);
	    }
	    else {
	    	secretKey = secretKeyHelper.getKey();
	    }
	 
	    FileEncrypterDecrypter fileEncrypterDecrypter = new FileEncrypterDecrypter(secretKey, "AES/CBC/PKCS5Padding");
	    fileEncrypterDecrypter.encrypt(originalContent, "baz.enc");
	 
	    SecretKey secretKey2 = secretKeyHelper.getKey();
	    FileEncrypterDecrypter fileEncrypterDecrypter2 = new FileEncrypterDecrypter(secretKey2, "AES/CBC/PKCS5Padding");
	    String decryptedContent = fileEncrypterDecrypter2.decrypt("baz.enc");
	    assertThat(decryptedContent, is(originalContent));
	 
	    new File("baz.enc").delete(); // cleanup
	}

}
