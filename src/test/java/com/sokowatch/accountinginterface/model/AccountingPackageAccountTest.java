package com.sokowatch.accountinginterface.model;

import java.util.Currency;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.jupiter.api.Test;

public class AccountingPackageAccountTest {
	@Test
	public void testEqualsAndHasCode() {
		AccountingPackageAccount account1 = new AccountingPackageAccount();
		account1.setId("1");
		account1.setNum("12345");
		account1.setCurrency(Currency.getInstance("USD"));
		
		AccountingPackageAccount account2 = new AccountingPackageAccount();
		account2.setId(account1.getId());
		account2.setNum(account1.getNum());
		account2.setCurrency(account1.getCurrency());
		
		assertThat(account1.equals(account2), is(true));
		assertThat(account1.hashCode() == account2.hashCode(), is(true));
		
		account2.setId("2");
		assertThat(account1.equals(account2), is(false));
		assertThat(account1.hashCode() == account2.hashCode(), is(false));
		account2.setId(account1.getId());
		assertThat(account1.equals(account2), is(true));
		assertThat(account1.hashCode() == account2.hashCode(), is(true));
		
		account2.setNum("23456");
		assertThat(account1.equals(account2), is(false));
		assertThat(account1.hashCode() == account2.hashCode(), is(false));
		account2.setNum(account1.getNum());
		assertThat(account1.equals(account2), is(true));
		assertThat(account1.hashCode() == account2.hashCode(), is(true));
		
		account2.setCurrency(Currency.getInstance("KES"));
		assertThat(account1.equals(account2), is(true)); //currency is ignored for equality
		assertThat(account1.hashCode() == account2.hashCode(), is(true)); //currency is ignored for hashcode
		account2.setCurrency(account1.getCurrency());
		assertThat(account1.equals(account2), is(true));
		assertThat(account1.hashCode() == account2.hashCode(), is(true));
	}
	
	@Test
	public void testNullStatusInSetterSetsItToAnEmptyString() {
		AccountingPackageAccount account = new AccountingPackageAccount();
		account.setStatus(null);
		assertThat(account.getStatus().isEmpty(), is(true));
	}

}
