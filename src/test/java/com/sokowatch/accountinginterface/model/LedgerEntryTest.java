package com.sokowatch.accountinginterface.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class LedgerEntryTest {
	
	@Test
	public void testEqualsAndHasCode() {
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.of(2020,5,10));
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(new BigDecimal(4000));
		ledgerEntry2.setDate(LocalDate.of(2020,4,10));
		
		assertThat(ledgerEntry1.equals(ledgerEntry2), is(false));
		assertThat(ledgerEntry1.hashCode() == ledgerEntry2.hashCode(), is(false));
		
		ledgerEntry2.setAmount(ledgerEntry1.getAmount());
		assertThat(ledgerEntry1.equals(ledgerEntry2), is(false));
		assertThat(ledgerEntry1.hashCode() == ledgerEntry2.hashCode(), is(false));
		
		ledgerEntry2.setDate(ledgerEntry1.getDate());
		assertThat(ledgerEntry1.equals(ledgerEntry2), is(true));
		assertThat(ledgerEntry1.hashCode() == ledgerEntry2.hashCode(), is(true));
		
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		
		assertThat(ledgerEntry1.equals(ledgerEntry2), is(false));
		assertThat(ledgerEntry1.hashCode() == ledgerEntry2.hashCode(), is(false));
		
		ledgerEntry2.setAccountClassLocation(accountClassLocation);
		assertThat(ledgerEntry1.equals(ledgerEntry2), is(true));
		assertThat(ledgerEntry1.hashCode() == ledgerEntry2.hashCode(), is(true));
	}
}
