package com.sokowatch.accountinginterface.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import org.junit.jupiter.api.Test;

public class AccountClassLocationTest {
	
	@Test
	public void testAddLeddgerEntryToAccountClassLocationAllowsDuplicateLedgerEntryToBeAdded() { 
		//reason we are ok with a duplicate being added is because a ledger entry is just an account with class, location, amount and date.  There could be multiple entries that match that signature
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.now());
		
		assertThat(accountClassLocation.getLedgerEntries().contains(ledgerEntry1), is(false));
		assertThat(accountClassLocation.getLedgerEntries().size(), is(0));
		
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		
		assertThat(ledgerEntry1.getAccountClassLocation(), is(accountClassLocation));
		assertThat(accountClassLocation.getLedgerEntries().contains(ledgerEntry1), is(true));
		assertThat(accountClassLocation.getLedgerEntries().size(), is(1));
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(ledgerEntry1.getAmount());
		ledgerEntry1.setDate(ledgerEntry1.getDate());
		
		assertThat(accountClassLocation.getLedgerEntries().contains(ledgerEntry2), is(false));
		assertThat(accountClassLocation.getLedgerEntries().size(), is(1));
		
		ledgerEntry2.setAccountClassLocation(accountClassLocation);
		
		assertThat(ledgerEntry2.getAccountClassLocation(), is(accountClassLocation));
		assertThat(accountClassLocation.getLedgerEntries().contains(ledgerEntry1), is(true));
		assertThat(accountClassLocation.getLedgerEntries().contains(ledgerEntry2), is(true));
		assertThat(accountClassLocation.getLedgerEntries().size(), is(2));
	}
	
	@Test
	public void testEqualsAndHasCode() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountingPackageAccount accountingPackageAccount2 = new AccountingPackageAccount();
		accountingPackageAccount2.setId("2");
		accountingPackageAccount2.setNum("23456");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		accountClassLocation.setCompanyCurrency(Currency.getInstance("KES")); //this is the home currency of the entity in which the account resides
		
		AccountClassLocation accountClassLocation2 = new AccountClassLocation();
		accountClassLocation2.setAccountingPackageAccount(accountClassLocation.getAccountingPackageAccount());
		accountClassLocation2.setClassTag(accountClassLocation.getClassTag());
		accountClassLocation2.setAccountingLocation(accountClassLocation.getLocationTag());
		accountClassLocation2.setCompanyCurrency(Currency.getInstance("KES"));

		assertThat(accountClassLocation.equals(accountClassLocation2), is(true));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(true));
		
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount2);;
		assertThat(accountClassLocation.equals(accountClassLocation2), is(false));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(false));
		
		accountClassLocation.setAccountingPackageAccount(accountClassLocation2.getAccountingPackageAccount()); //set it back
		assertThat(accountClassLocation.equals(accountClassLocation2), is(true));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(true));
		accountClassLocation.setClassTag("Class 2");
		assertThat(accountClassLocation.equals(accountClassLocation2), is(false));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(false));
		
		accountClassLocation.setClassTag(accountClassLocation2.getClassTag()); //set it back
		assertThat(accountClassLocation.equals(accountClassLocation2), is(true));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(true));
		accountClassLocation.setAccountingLocation("Location 2");
		assertThat(accountClassLocation.equals(accountClassLocation2), is(false));
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(false));
		
		accountClassLocation.setAccountingLocation(accountClassLocation2.getLocationTag()); //set it back
		//now add a LedgerEntry to one of them
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.now());
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		
		assertThat(accountClassLocation.equals(accountClassLocation2), is(true)); //even though accountClassLocation has a ledgerentry, they are still equal because equality is not checked on the LedgerEntries
		assertThat(accountClassLocation.hashCode() == accountClassLocation2.hashCode(), is(true));
	}
	
	@Test
	public void testGetTotalAmountForMonthYear() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.of(2020,5,10));
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(new BigDecimal(4250.86));
		ledgerEntry2.setDate(LocalDate.of(2020,5,31));
		
		LedgerEntry ledgerEntry3 = new LedgerEntry();
		ledgerEntry3.setAmount(new BigDecimal(12.89));
		ledgerEntry3.setDate(LocalDate.of(2020,10,20));

		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		ledgerEntry2.setAccountClassLocation(accountClassLocation);
		ledgerEntry3.setAccountClassLocation(accountClassLocation);
		
		BigDecimal amountForJan2020 = accountClassLocation.getTotalAmountForMonthYear(LocalDate.of(2020,1,31));
		BigDecimal amountForMay2020 = accountClassLocation.getTotalAmountForMonthYear(LocalDate.of(2020,5,31));
		BigDecimal amountForOct2020 = accountClassLocation.getTotalAmountForMonthYear(LocalDate.of(2020,10,15)); //I specifically pass in a random day of the month to ensure the method converts it to the last day of the month
		
		assertThat(amountForJan2020, is(BigDecimal.ZERO));
		assertThat(amountForMay2020, is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount())));
		assertThat(amountForOct2020, is(ledgerEntry3.getAmount()));
	}
	
	@Test
	public void testGetClosingBalanceForMonthYear() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.of(2020,5,10));
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(new BigDecimal(4250.86));
		ledgerEntry2.setDate(LocalDate.of(2020,5,31));
		
		LedgerEntry ledgerEntry3 = new LedgerEntry();
		ledgerEntry3.setAmount(new BigDecimal(12.89));
		ledgerEntry3.setDate(LocalDate.of(2020,10,20));

		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		ledgerEntry2.setAccountClassLocation(accountClassLocation);
		ledgerEntry3.setAccountClassLocation(accountClassLocation);
		
		BigDecimal closingBalanceForJan2020 = accountClassLocation.getClosingBalanceForMonthYear(LocalDate.of(2020,1,31));
		BigDecimal closingBalanceForMay2020 = accountClassLocation.getClosingBalanceForMonthYear(LocalDate.of(2020,5,31));
		BigDecimal closingBalanceForOct2020 = accountClassLocation.getClosingBalanceForMonthYear(LocalDate.of(2020,10,15)); //I specifically pass in a random day of the month to ensure the method converts it to the last day of the month
		
		assertThat(closingBalanceForJan2020, is(BigDecimal.ZERO));
		assertThat(closingBalanceForMay2020, is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount())));
		assertThat(closingBalanceForOct2020, is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount().add(ledgerEntry3.getAmount()))));
	}
	
	@Test
	public void testAccountingClassAndAccountingLocationDefaultsToNotSetIfEmpty() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("");
		accountClassLocation.setAccountingLocation("");
		
		assertThat(accountClassLocation.getClassTag().equals("Not Set"), is(true));
		assertThat(accountClassLocation.getLocationTag().equals("Not Set"), is(true));
		
	}
}
