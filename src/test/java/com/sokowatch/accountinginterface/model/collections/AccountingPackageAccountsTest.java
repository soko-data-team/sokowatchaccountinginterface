package com.sokowatch.accountinginterface.model.collections;

import org.junit.jupiter.api.Test;
import com.sokowatch.accountinginterface.model.AccountingPackageAccount;
import com.sokowatch.accountinginterface.model.collection.AccountingPackageAccounts;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.Currency;

public class AccountingPackageAccountsTest {
	@Test
	public void testAddAccountingPackageAccountDoesNotAllowDuplicatesToBeAdded() {
		AccountingPackageAccount accountingPackageAccount1 = new AccountingPackageAccount();
		accountingPackageAccount1.setNum("12345");
		accountingPackageAccount1.setId("123");
		accountingPackageAccount1.setCurrency(Currency.getInstance("USD"));
		
		AccountingPackageAccounts accountingPackageAccounts = new AccountingPackageAccounts();
		
		assertThat(accountingPackageAccounts.size(), is(0));
		assertThat(accountingPackageAccounts.contains(accountingPackageAccount1), is(false));
		
		accountingPackageAccounts.add(accountingPackageAccount1);
		
		assertThat(accountingPackageAccounts.size(), is(1));
		assertThat(accountingPackageAccounts.contains(accountingPackageAccount1), is(true));
		
		AccountingPackageAccount accountingPackageAccount2 = new AccountingPackageAccount();
		accountingPackageAccount2.setNum(accountingPackageAccount1.getNum());
		accountingPackageAccount2.setId(accountingPackageAccount1.getId());
		accountingPackageAccount2.setCurrency(accountingPackageAccount1.getCurrency());
		
		accountingPackageAccounts.add(accountingPackageAccount2);
		
		assertThat(accountingPackageAccounts.size(), is(1));
		assertThat(accountingPackageAccounts.contains(accountingPackageAccount1), is(true));
		assertThat(accountingPackageAccounts.contains(accountingPackageAccount2), is(true)); //this will be true because they have the same id and num
	}
	
	@Test
	public void testGetAccountingPackageAccountWithNum() {
		AccountingPackageAccount accountingPackageAccount1 = new AccountingPackageAccount();
		accountingPackageAccount1.setNum("12345");
		accountingPackageAccount1.setId("123");
		accountingPackageAccount1.setCurrency(Currency.getInstance("USD"));
		
		AccountingPackageAccounts accountingPackageAccounts = new AccountingPackageAccounts();
		accountingPackageAccounts.add(accountingPackageAccount1);
		
		AccountingPackageAccount accountingPackageAccount2 = accountingPackageAccounts.getAccountingPackageAccountWithNum(accountingPackageAccount1.getNum());
		assertThat(accountingPackageAccount1.equals(accountingPackageAccount2), is(true));
	}
	
	@Test
	public void testWhenAccountPackageAccountsHasAccountsWithNoNumThenGetAccountingPackageWithNumHandlesIt() {
		AccountingPackageAccount accountingPackageAccount1 = new AccountingPackageAccount();
		//accountingPackageAccount2.setNum("12345");
		accountingPackageAccount1.setId("234");
		accountingPackageAccount1.setCurrency(Currency.getInstance("USD"));
		
		AccountingPackageAccounts accountingPackageAccounts = new AccountingPackageAccounts();
		accountingPackageAccounts.add(accountingPackageAccount1);
		
		AccountingPackageAccount accountingPackageAccount3 = accountingPackageAccounts.getAccountingPackageAccountWithNum("1234");
		assertThat(accountingPackageAccount1.equals(accountingPackageAccount3), is(false));
		
	}

}
