package com.sokowatch.accountinginterface.model.collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import com.sokowatch.accountinginterface.model.AccountClassLocation;
import com.sokowatch.accountinginterface.model.AccountingPackageAccount;
import com.sokowatch.accountinginterface.model.LedgerEntry;
import com.sokowatch.accountinginterface.model.collection.AccountClassLocations;

public class AccountClassLocationsTest {
	@Test
	public void testAddAccountClassLocationDoesNotAllowDuplicatesToBeAdded() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		AccountClassLocation accountClassLocation1 = new AccountClassLocation();
		accountClassLocation1.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation1.setClassTag("Class");
		accountClassLocation1.setAccountingLocation("AccountLocation");
		
		AccountClassLocations accountClassLocations = new AccountClassLocations();
		
		assertThat(accountClassLocations.size(), is(0));
		assertThat(accountClassLocations.contains(accountClassLocation1), is(false));
		
		accountClassLocations.add(accountClassLocation1);
		
		assertThat(accountClassLocations.size(), is(1));
		assertThat(accountClassLocations.contains(accountClassLocation1), is(true));
		
		AccountClassLocation accountClassLocation2 = new AccountClassLocation();
		accountClassLocation2.setAccountingPackageAccount(accountClassLocation1.getAccountingPackageAccount());
		accountClassLocation2.setClassTag(accountClassLocation1.getClassTag());
		accountClassLocation2.setAccountingLocation(accountClassLocation1.getLocationTag());
		
		accountClassLocations.add(accountClassLocation2);
		
		assertThat(accountClassLocations.size(), is(1));
		assertThat(accountClassLocations.contains(accountClassLocation1), is(true));
		assertThat(accountClassLocations.contains(accountClassLocation2), is(true)); //this will be true because contains looks at the accountnum, class and location and because they are the same it comes back true.  But it has not added another item to the collection
	}
	
	@Test
	public void testAddAccountClassLocationDoesNotAllowDuplicatesToBeAddedEvenIfTheNewAccountClassLocationHasDifferentLedgerEntries() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.now());
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		
		AccountClassLocations accountClassLocations = new AccountClassLocations();
		assertThat(accountClassLocations.contains(accountClassLocation), is(false));
		assertThat(accountClassLocations.size(), is(0));

		accountClassLocations.add(accountClassLocation);
		
		assertThat(accountClassLocations.contains(accountClassLocation), is(true));
		assertThat(accountClassLocations.size(), is(1));
		
		AccountClassLocation accountClassLocation2 = new AccountClassLocation();
		accountClassLocation2.setAccountingPackageAccount(accountClassLocation.getAccountingPackageAccount());
		accountClassLocation2.setClassTag(accountClassLocation.getClassTag());
		accountClassLocation2.setAccountingLocation(accountClassLocation.getLocationTag());
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(new BigDecimal(8000)); //diff amount
		ledgerEntry2.setDate(LocalDate.now());
		ledgerEntry2.setAccountClassLocation(accountClassLocation2);
		
		accountClassLocations.add(accountClassLocation2);
		
		assertThat(accountClassLocations.contains(accountClassLocation), is(true));
		assertThat(accountClassLocations.size(), is(1));
	}
	
	@Test
	public void testAddAccountClassLocationThatAlreadyExistsButWithDifferentLedgerEntriesAddsTheNewLedgerEntriesToTheExistingAccountClassLocation() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Class");
		accountClassLocation.setAccountingLocation("AccountLocation");
		
		LedgerEntry ledgerEntry1 = new LedgerEntry();
		ledgerEntry1.setAmount(new BigDecimal(5000));
		ledgerEntry1.setDate(LocalDate.now().minusMonths(1L));
		ledgerEntry1.setAccountClassLocation(accountClassLocation);
		
		LedgerEntry ledgerEntry2 = new LedgerEntry();
		ledgerEntry2.setAmount(new BigDecimal(8000)); //diff amount
		ledgerEntry2.setDate(ledgerEntry1.getDate()); //same date
		ledgerEntry2.setAccountClassLocation(ledgerEntry1.getAccountClassLocation()); //same accountClassLocation

		AccountClassLocations accountClassLocations = new AccountClassLocations();
		accountClassLocations.addOrUpdateAccountClassLocation(accountClassLocation);
		
		assertThat(accountClassLocations.contains(accountClassLocation), is(true));
		assertThat(accountClassLocations.size(), is(1));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry1), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry2), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().size(), is(2));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getTotalAmountForMonthYear(ledgerEntry1.getDate()), is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount())));
		
		//Now we run a new month that creates a new AccountClassLocation but that already exists in AccountClassLocations.  In this case, it should recognise that it already exists and just add the new ledgerEntries
		AccountClassLocation accountClassLocation2 = new AccountClassLocation();
		accountClassLocation2.setAccountingPackageAccount(accountingPackageAccount);  //same accountingPackageAccount
		accountClassLocation2.setClassTag(accountClassLocation.getClassTag()); //same accountingClass
		accountClassLocation2.setAccountingLocation(accountClassLocation.getLocationTag()); //same accountingLocation
		
		LedgerEntry ledgerEntry3 = new LedgerEntry();
		ledgerEntry3.setAmount(new BigDecimal(1000));
		ledgerEntry3.setDate(LocalDate.now()); //diff date
		ledgerEntry3.setAccountClassLocation(accountClassLocation2);
		
		accountClassLocations.addOrUpdateAccountClassLocation(accountClassLocation2); //this should not create a new accountClassLocation in the list because it has the same accountingPackageAccount as what already exists in the list
		
		assertThat(accountClassLocations.contains(accountClassLocation), is(true));
		assertThat(accountClassLocations.contains(accountClassLocation2), is(true)); //accountClassLocation1 and 2 are identical based on the equals method, therefore contains() should return true for both
		assertThat(accountClassLocations.size(), is(1)); //but accountClassLocations should only still have 1 item
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry1), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry2), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry3), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().size(), is(3));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getTotalAmountForMonthYear(ledgerEntry1.getDate()), is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount())));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getTotalAmountForMonthYear(ledgerEntry3.getDate()), is(ledgerEntry3.getAmount()));
		
		//Now we run a new month that creates a new AccountClassLocation that has come up for the first time in AccountClassLocations.  In this case, it should recognise that it does not already exists and add the new AccountClassLocation with its ledgerEntries
		AccountingPackageAccount accountingPackageAccount2 = new AccountingPackageAccount();
		accountingPackageAccount2.setId("2");
		accountingPackageAccount2.setNum("23456");
		
		AccountClassLocation accountClassLocation3 = new AccountClassLocation();
		accountClassLocation3.setAccountingPackageAccount(accountingPackageAccount2);  //different accountingPackageAccount
		accountClassLocation3.setClassTag(accountClassLocation.getClassTag()); //doesn't matter that it is the same accountingClass
		accountClassLocation3.setAccountingLocation(accountClassLocation.getLocationTag()); //doesn't matter that it is the same accountingLocation
		
		LedgerEntry ledgerEntry4 = new LedgerEntry();
		ledgerEntry4.setAmount(new BigDecimal(9000));
		ledgerEntry4.setDate(LocalDate.now()); //same date
		ledgerEntry4.setAccountClassLocation(accountClassLocation3);
		
		accountClassLocations.addOrUpdateAccountClassLocation(accountClassLocation3); //this should create a new accountClassLocation in the list because it has a different accountingPackageAccount
		
		assertThat(accountClassLocations.contains(accountClassLocation), is(true));
		assertThat(accountClassLocations.contains(accountClassLocation3), is(true)); //accountClassLocation1 and 2 are identical based on the equals method, therefore contains() should return true for both
		assertThat(accountClassLocations.size(), is(2)); //accountClassLocations have added another item
		//Make sure accountClassLocation hasn't changed at all (ie these are the same tests as above)
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry1), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry2), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().contains(ledgerEntry3), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getLedgerEntries().size(), is(3));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getTotalAmountForMonthYear(ledgerEntry1.getDate()), is(ledgerEntry1.getAmount().add(ledgerEntry2.getAmount())));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation).getTotalAmountForMonthYear(ledgerEntry3.getDate()), is(ledgerEntry3.getAmount()));
		
		//Now check accountClassLocation3
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation3).getLedgerEntries().contains(ledgerEntry4), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation3).getLedgerEntries().size(), is(1));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation3).getTotalAmountForMonthYear(ledgerEntry4.getDate()), is(ledgerEntry4.getAmount()));
	}
	
	
	@Test
	public void testGetAccountClassLocation() {
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountClassLocation accountClassLocation = new AccountClassLocation();
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccount);
		accountClassLocation.setClassTag("Accounting Class");
		accountClassLocation.setAccountingLocation("Accounting Location");
		
		AccountClassLocations accountClassLocations = new AccountClassLocations();
		accountClassLocations.add(accountClassLocation);
		
		AccountClassLocation accountClassLocation2 = new AccountClassLocation();
		accountClassLocation2.setAccountingPackageAccount(accountClassLocation.getAccountingPackageAccount());
		accountClassLocation2.setClassTag(accountClassLocation.getClassTag());
		accountClassLocation2.setAccountingLocation(accountClassLocation.getLocationTag());
		
		AccountClassLocation accountClassLocation3 = new AccountClassLocation();
		accountClassLocation3.setAccountingPackageAccount(accountClassLocation.getAccountingPackageAccount());
		accountClassLocation3.setClassTag(accountClassLocation.getClassTag());
		accountClassLocation3.setAccountingLocation("Something Different");
		
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation2).equals(accountClassLocation), is(true));
		assertThat(accountClassLocations.getAccountClassLocation(accountClassLocation3), is(nullValue()));
	}
	
	/*@Test
	public void testPopulateFromReportRows() {
		String companyName = "ACME Inc";
		Currency companyCurrency = Currency.getInstance("USD");
		
		AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
		accountingPackageAccount.setId("1");
		accountingPackageAccount.setNum("12345");
		
		AccountingPackageAccounts accountingPackageAccounts = new AccountingPackageAccounts();
		accountingPackageAccounts.add(accountingPackageAccount);
		
		Row row1 = new Row();
		row1.setType(RowTypeEnum.SECTION);
		
		Row row2 = new Row();
		row2.setType(RowTypeEnum.DATA);
		Rows rows2 = new Rows();
		
		row1.setRows(row2);
		
		
		List<Row> rowList1 = new ArrayList<Row>();
		rowList1.add(row1);
		
		
		
		Rows rows1 = new Rows();
		rows1.setRow(rowList1);
		
		
		AccountClassLocations accountClassLocations = new AccountClassLocations();
		accountClassLocations.populateFromReportRows(rows1, accountingPackageAccounts, companyName, companyCurrency);
	}
	*/

}
