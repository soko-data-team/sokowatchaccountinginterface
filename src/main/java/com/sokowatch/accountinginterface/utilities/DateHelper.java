package com.sokowatch.accountinginterface.utilities;

import java.time.LocalDate;

public class DateHelper {
	
	public static LocalDate getConstrainedDate(LocalDate calculatedDate, LocalDate maxDate) {
		if(calculatedDate.compareTo(maxDate)>0) {
			calculatedDate = maxDate;
		}
		
		return calculatedDate;
	}
}
