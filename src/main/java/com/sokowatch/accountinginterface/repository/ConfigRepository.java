package com.sokowatch.accountinginterface.repository;

import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class ConfigRepository {
	@Value( "${ConfigFileLocation}" )
	private String configFileLocation;
	private static final Logger logger = Logger.getLogger(ConfigRepository.class);
	
	public String findConfigValue(String configValue) throws IOException {
		return this.readFromFile(configFileLocation + configValue + ".txt");
	}
	//This should probably all go into a DB, but I didn't think it was worth having a DB for this
	private String readFromFile(String fileName)  throws IOException {
		FileInputStream inputStream = new FileInputStream(fileName);    
		return IOUtils.toString(inputStream, "UTF-8");
	}
}
