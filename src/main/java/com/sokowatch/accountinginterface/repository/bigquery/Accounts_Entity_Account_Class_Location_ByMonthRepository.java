package com.sokowatch.accountinginterface.repository.bigquery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryError;
import com.google.cloud.bigquery.InsertAllRequest;
import com.google.cloud.bigquery.InsertAllResponse;
import com.google.cloud.bigquery.Job;
import com.google.cloud.bigquery.JobId;
import com.google.cloud.bigquery.JobInfo;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableId;
import com.google.cloud.bigquery.TableResult;
import com.sokowatch.accountinginterface.dto.ObjectToRowContentFacade;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;
import com.sokowatch.accountinginterface.helper.BigQueryServiceHelper;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;

@Component
public class Accounts_Entity_Account_Class_Location_ByMonthRepository {
	private static final Logger logger = Logger.getLogger(Accounts_Entity_Account_Class_Location_ByMonthRepository.class);
	
	@Autowired
	BigQueryServiceHelper bigQueryHelper;
	@Autowired
	ConfigServiceImpl configServiceImpl;
	
	public Accounts_Entity_Account_Class_Location_ByMonthRepository() {
	}

	public TableResult findAll() throws InterruptedException, FileNotFoundException, IOException {
		logger.debug("Finding all Data for table from DB");
		
		BigQuery bigquery = bigQueryHelper.getDataService();
		
		QueryJobConfiguration queryConfig =
				QueryJobConfiguration.newBuilder("SELECT * FROM " + configServiceImpl.getConfigValue("BigQueryDataSetID") + "." + configServiceImpl.getConfigValue("BigQueryOutputTableName") + " LIMIT 10")
				.setUseLegacySql(false)
				.build();

		// Create a job ID so that we can safely retry.
		JobId jobId = JobId.of(UUID.randomUUID().toString());
		Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

		// Wait for the query to complete.
		queryJob = queryJob.waitFor();

		// Check for errors
		if (queryJob == null) {
			throw new RuntimeException("Job no longer exists");
		} else if (queryJob.getStatus().getError() != null) {
			// You can also look at queryJob.getStatus().getExecutionErrors() for all
			// errors, not just the latest one.
			throw new RuntimeException(queryJob.getStatus().getError().toString());
		}

		//Strictly speaking, you should convert this into an array if Accounts_Entity_Account_Class_Location objects.
		//but I am only really using it for testing now.  If you use it in prod
		//come back and refactor this
		return queryJob.getQueryResults();
	}
	
	public void runTableInsertRowsWithoutRowIds(List<SummaryGLLineDTO> dtos) throws Exception {
		
		String dataRunId = UUID.randomUUID().toString();
		
		List<InsertAllRequest.RowToInsert> rowContent = new ArrayList<>();
		
		// Create rows to insert
		Iterator<SummaryGLLineDTO> dtoIterator = dtos.iterator();
		int counter = 0;
		while(dtoIterator.hasNext()) {
			counter++;
			SummaryGLLineDTO dto = dtoIterator.next();
			Map<String, Object> map = ObjectToRowContentFacade.convertSummaryGLLineDTOToMap(dto, dataRunId);
			rowContent.add(InsertAllRequest.RowToInsert.of(map));
			
			if(counter == 500) { 
				tableInsertRowsWithoutRowIds(rowContent);
				rowContent.clear();
				counter = 0;
			}
		}
		/*
		Because I call rowContent.clear() above, it means that I can be confident that I have not already submitted
		this rowContent.  The below tableInsertRowsWithoutRowIds() call will only be made when the final batch is less
		than 500, in which case the above tableInsertRowsWithoutRowIds() won't have been called, so this catches the 
		residual 
		*/ 
		if(!rowContent.isEmpty()) {
			tableInsertRowsWithoutRowIds(rowContent);
		}
	}
	
	private void tableInsertRowsWithoutRowIds(Iterable<InsertAllRequest.RowToInsert> rows) throws Exception {
		//try {
		BigQuery bigquery = bigQueryHelper.getDataService();

		TableId tableId = TableId.of(configServiceImpl.getConfigValue("BigQueryDataSetID"), 
				configServiceImpl.getConfigValue("BigQueryOutputTableName"));
		
		InsertAllResponse response =
				bigquery.insertAll(InsertAllRequest.newBuilder(tableId).setRows(rows).build());

		if (response.hasErrors()) {
			// If any of the insertions failed, this lets you inspect the errors
			for (Map.Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
				logger.debug("BigQuery Insert Response error: \n" + entry.getValue());
			}
			throw new Exception("Error uploading data to BigQuery");
		}
	}
}
