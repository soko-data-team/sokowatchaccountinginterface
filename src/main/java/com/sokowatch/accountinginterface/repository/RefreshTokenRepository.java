package com.sokowatch.accountinginterface.repository;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sokowatch.accountinginterface.encrypt.FileEncrypterDecrypter;
import com.sokowatch.accountinginterface.encrypt.SecretKeyHelper;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;

@Component
public class RefreshTokenRepository {
	@Autowired
	SecretKeyHelper secretKeyHelper;
	@Autowired
	ConfigServiceImpl configServiceImpl;
	private static final Logger logger = Logger.getLogger(RefreshTokenRepository.class);
	
	//These tokens should probably be in a DB, but I didn't think it was worth setting up a full DB just for this. So I have ket them as encrypted text files
	
	public String findTokenForRealmId(String realmId) throws IOException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
		logger.debug("Getting Refresh Token for RealmId: " + realmId);
		
		SecretKey secretKey = secretKeyHelper.getKey();
		FileEncrypterDecrypter fileEncrypterDecrypter = new FileEncrypterDecrypter(secretKey, "AES/CBC/PKCS5Padding");
		String unencryptedToken = fileEncrypterDecrypter.decrypt(configServiceImpl.getConfigValue("OutputFileLocation") + "RefreshToken_" + realmId + ".enc");
		
		logger.debug("Refresh Token for RealmId: " + realmId + " is: " + unencryptedToken);
		
		return unencryptedToken;
	}
	
	public void saveTokenForRealmId(String realmId, String token) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException {
		logger.debug("Saving new Refresh Token for RealmId: " + realmId);
		
		SecretKey secretKey;
	    if(! secretKeyHelper.keyExists()) {
	    	secretKey = secretKeyHelper.generateKey();
	    	secretKeyHelper.saveKey(secretKey);
	    }
	    else {
	    	secretKey = secretKeyHelper.getKey();
	    }
		
	    FileEncrypterDecrypter fileEncrypterDecrypter = new FileEncrypterDecrypter(secretKey, "AES/CBC/PKCS5Padding");
	    fileEncrypterDecrypter.encrypt(token, configServiceImpl.getConfigValue("OutputFileLocation") + "RefreshToken_" + realmId + ".enc");

	    logger.debug("Refresh Token for RealmId: " + realmId + " updated to: " + token);
	}
}
