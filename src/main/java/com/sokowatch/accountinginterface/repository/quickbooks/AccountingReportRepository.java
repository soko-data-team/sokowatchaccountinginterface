package com.sokowatch.accountinginterface.repository.quickbooks;

import java.io.IOException;
import java.time.LocalDate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.intuit.ipp.data.Report;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.ReportName;
import com.intuit.ipp.services.ReportService;
import com.sokowatch.accountinginterface.helper.QBOServiceHelper;
import com.sokowatch.accountinginterface.service.quickbooks.GeneralLedgerServiceImpl;

@Component
public class AccountingReportRepository {
	@Autowired
	public QBOServiceHelper helper;
	
	private static final Logger logger = Logger.getLogger(GeneralLedgerServiceImpl.class);
	
	public AccountingReportRepository() {
	}
	
	public Report findLedgerReportForTimePeriod(String realmId, String accessToken,
			LocalDate fromDate, LocalDate toDate, Boolean companyIsMultiCurrency) throws FMSException, IOException {
		
		//TODO This badly needs a unit test, but the problem is it requires a login to QB's.  Could mock it out.
		
		logger.debug("Getting Ledger Data for " + fromDate.toString() + " to " + toDate.toString());
		ReportService service = helper.getReportService(realmId, accessToken);
		service.setStart_date(fromDate.toString());
		service.setEnd_date(toDate.toString());
		
		String amountColumnToUse = "subt_nat_home_amount";
		if(!companyIsMultiCurrency) {
			amountColumnToUse = "subt_nat_amount";
		}
			
		/*** Remember, if you change the columns in the below, you need to update the code in the isADataRowForInclusion() and the addFromRow() methods in AccountClassLocations class.
		 * Be very wary of adding columns because QB throttles the data based on number of cells.  So if you add columns, it quickly explodes the number of cells
		 ****/
		//service.setColumns("tx_date,account_num,"+amountColumnToUse+",klass_name,dept_name,txn_type,doc_num,cust_name,is_ap_paid,is_ar_paid,is_cleared,item_name,memo,name,split_acc,tx_date,vend_name");
		service.setColumns("tx_date,account_num,"+amountColumnToUse+",klass_name,dept_name,txn_type");
		//logger.debug("ReportName -> name: " + ledger.getHeader().getReportName().toLowerCase());
		return service.executeReport(ReportName.GENERALLEDGER.toString());
	}
}
