package com.sokowatch.accountinginterface.repository.quickbooks;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.intuit.ipp.data.CompanyInfo;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.sokowatch.accountinginterface.helper.QBOServiceHelper;
import com.sokowatch.accountinginterface.service.quickbooks.GeneralLedgerServiceImpl;

@Component
public class CompanyInfoRepository {
	@Autowired
	public QBOServiceHelper helper;
	
	private static final Logger logger = Logger.getLogger(GeneralLedgerServiceImpl.class);
	
	public CompanyInfoRepository() {
	}
	
	public CompanyInfo findCompanyInfo(String realmId, String accessToken) throws FMSException, IOException {
		logger.debug("Getting Company Info");
		// Get DataService
		DataService service = helper.getDataService(realmId, accessToken);
				
		// get all company info
		String sql = "select * from companyinfo";
		QueryResult queryResult = service.executeQuery(sql);
		return (CompanyInfo)queryResult.getEntities().get(0);
	}

}
