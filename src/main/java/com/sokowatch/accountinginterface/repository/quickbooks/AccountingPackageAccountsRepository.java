package com.sokowatch.accountinginterface.repository.quickbooks;

import java.util.Currency;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.intuit.ipp.core.IEntity;
import com.intuit.ipp.data.Account;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.sokowatch.accountinginterface.helper.QBOServiceHelper;
import com.sokowatch.accountinginterface.model.AccountingPackageAccount;
import com.sokowatch.accountinginterface.model.collection.AccountingPackageAccounts;

@Component
public class AccountingPackageAccountsRepository {
	@Autowired
	public QBOServiceHelper helper;
	
	private static final Logger logger = Logger.getLogger(AccountingPackageAccountsRepository.class);
	
	public AccountingPackageAccountsRepository() {
	}
	
	public AccountingPackageAccounts findAccountingPackageAccounts(String realmId, String accessToken) throws Exception {
		// Get DataService
		DataService service = helper.getDataService(realmId, accessToken);

		// get all accounts
		String sql = "select * from account WHERE Active IN (true, false) MAXRESULTS 1000";
		QueryResult queryResult = service.executeQuery(sql);
		int count = queryResult.getEntities().size();
		logger.info("Total number of accounts: " + count);

		AccountingPackageAccounts accountingPackageAccounts = new AccountingPackageAccounts();

		for (IEntity account : queryResult.getEntities()) {
			if(((Account)account).getAcctNum() == null && !((Account)account).getName().contains("deleted")) {
				throw new Exception("Data load failed because Account with name [" + ((Account)account).getName() + "] has been added without an account number.  Please add account number to the account in Quickbooks and add to the account mapping tab in the maanagement accounts");
			}			
			AccountingPackageAccount accountingPackageAccount = new AccountingPackageAccount();
			accountingPackageAccount.setId(((Account)account).getId());
			accountingPackageAccount.setNum(((Account)account).getAcctNum());
			accountingPackageAccount.setName(((Account)account).getName());
			accountingPackageAccount.setIsCurrencyValue(true);
			accountingPackageAccount.setClassification(((Account)account).getClassification().toString());
			accountingPackageAccount.setAccountSubType(((Account)account).getAccountSubType().toString());
			if(((Account)account).getStatus() == null) { accountingPackageAccount.setStatus(null); }
			else { accountingPackageAccount.setStatus(((Account)account).getStatus().toString()); }
			accountingPackageAccount.setCurrency(Currency.getInstance(((Account)account).getCurrencyRef().getValue()));
			accountingPackageAccount.setCurrentBalance(((Account)account).getCurrentBalance());
			accountingPackageAccount.setCurrentBalanceWitthSubAccounts(((Account)account).getCurrentBalanceWithSubAccounts());

			accountingPackageAccounts.add(accountingPackageAccount);
		}

		return accountingPackageAccounts;
	}
}
