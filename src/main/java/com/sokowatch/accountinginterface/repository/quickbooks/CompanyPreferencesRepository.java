package com.sokowatch.accountinginterface.repository.quickbooks;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intuit.ipp.data.Preferences;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.sokowatch.accountinginterface.helper.QBOServiceHelper;
import com.sokowatch.accountinginterface.service.quickbooks.GeneralLedgerServiceImpl;

@Component
public class CompanyPreferencesRepository {
	@Autowired
	public QBOServiceHelper helper;
	
	private static final Logger logger = Logger.getLogger(GeneralLedgerServiceImpl.class);
	
	public CompanyPreferencesRepository() {
	}
	
	public Preferences findCompanyPreferences(String realmId, String accessToken) throws FMSException, IOException {
		logger.debug("Getting Company Preferences");
		// Get DataService
		DataService service = helper.getDataService(realmId, accessToken);
				
		// get all preferences info
		String sql = "select * from preferences";
		QueryResult queryResult = service.executeQuery(sql);
		return (Preferences)queryResult.getEntities().get(0);
	}
}
