package com.sokowatch.accountinginterface.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.sokowatch.accountinginterface.dto.LedgerEntryDTO;
import com.sokowatch.accountinginterface.dto.ObjectToDtoFacade;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;
import com.sokowatch.accountinginterface.utilities.CSVUtils;

public class CSVHelper {
	private static final Logger logger = Logger.getLogger(CSVHelper.class);

	static public String toSummaryGLLineCSVFile(List<SummaryGLLineDTO> dtos, String filePathName) throws IOException {
		/*
		 *Note, if you change the format of the below CSV output file, you need to change the format of the
		 *Base data csv file used for the integration test and you also need to change the compareCSVs method below
		 *and the ObjectToDtoFacade.convertToDtoFromCSVLine() method
		 */
		FileWriter writer = new FileWriter(filePathName);
		//for header
		CSVUtils.writeLine(writer, Arrays.asList("Data Run DateStamp", "DataVersion", "Company", "Date", "MetricID", "UniqueMetricID", "Metric Name", "Is Currency Value", "Metric Currency", "Company Currency", "Classification", "Class Tag", "Location Tag", "Amount", "Closing Balance"));
		for (SummaryGLLineDTO d : dtos) {
			List<String> list = new ArrayList<>();
			list.add(d.getDataRunDateStamp());
			list.add(d.getDataVersion().toString());
			list.add(escapeSpecialCharacters(d.getCompanyName()));
			list.add(d.getDate());
			list.add(escapeSpecialCharacters(d.getAccountNum()));
			list.add(escapeSpecialCharacters(d.getAccountNum()+"-"+d.getCompanyName()));
			list.add(escapeSpecialCharacters(d.getAccountName()));
			list.add(d.getIsCurrencyValue().toString());
			list.add(d.getAccountCurrency().getCurrencyCode());
			list.add(escapeSpecialCharacters(d.getCompanyCurrency().getCurrencyCode()));
			list.add(escapeSpecialCharacters(d.getAccountClassification()));
			list.add(escapeSpecialCharacters(d.getAccountingClass()));
			list.add(escapeSpecialCharacters(d.getAccountingLocation()));
			list.add(String.valueOf(d.getAmount()));
			list.add(String.valueOf(d.getClosingBalance()));

			CSVUtils.writeLine(writer, list);

			//try custom separator and quote.
			//CSVUtils.writeLine(writer, list, '|', '\"');
		}

		writer.flush();
		writer.close();

		return "Success";
	}
	
	static public String toJournalEntryCSVFile(List<LedgerEntryDTO> dtos, String filePathName) throws IOException {
		//try {
		FileWriter writer = new FileWriter(filePathName, true);
		//for header
		//CSVUtils.writeLine(writer, Arrays.asList("Data Run DateStamp", "DataVersion", "Company", "Date", "MetricID", "UniqueMetricID", "Metric Name", "Is Currency Value", "Metric Currency", "Company Currency", "Classification", "Class Tag", "Location Tag", "Amount", "Closing Balance"));
		for (LedgerEntryDTO d : dtos) {
			List<String> list = new ArrayList<>();

			list.add(d.getDataRunDateStamp());
			list.add(escapeSpecialCharacters(d.getCompanyName()));
			list.add(escapeSpecialCharacters(d.getAccountNum()));
			list.add(escapeSpecialCharacters(d.getAccountName()));
			list.add(escapeSpecialCharacters(d.getAccountClassification()));
			list.add(escapeSpecialCharacters(d.getAccountSubType()));
			list.add(d.getDate());
			list.add(escapeSpecialCharacters(d.getAccountingClass()));
			list.add(escapeSpecialCharacters(d.getAccountingLocation()));
			list.add(String.valueOf(d.getAmount()));
			list.add(escapeSpecialCharacters(d.getTransactionType()));
			//list.add(escapeSpecialCharacters(d.getDocNumber()));
			//list.add(escapeSpecialCharacters(d.getCustomerName()));
			//list.add(escapeSpecialCharacters(d.getApPaidStatus()));
			//list.add(escapeSpecialCharacters(d.getArPaidStatus()));
			//list.add(escapeSpecialCharacters(d.getClearedStatus()));
			//list.add(escapeSpecialCharacters(d.getProductOrServiceName()));
			//list.add(escapeSpecialCharacters(d.getMemo()));
			//list.add(escapeSpecialCharacters(d.getName()));
			list.add(escapeSpecialCharacters(d.getSplitAccount()));
			//list.add(escapeSpecialCharacters(d.getSupplierName()));
			list.add(escapeSpecialCharacters(d.getSuspectedErrorHint()));
			CSVUtils.writeLine(writer, list);
		}

		writer.flush();
		writer.close();
		/*}
		catch (IOException ex) {
			logger.error("Could not write to CSV: " + ex.getMessage());
			return "Failed";
		}*/
		return "Success";
	}
	
	static public void replaceFileWithTemplateFile(String fileToDeleteFilePathName, String templateFilePathName) throws IOException {
		File fileToDelete = new File(fileToDeleteFilePathName);
		fileToDelete.delete();
		File templateFile = new File(templateFilePathName);
		
		FileUtils.copyFile(templateFile, fileToDelete);
	}
	
	public static Boolean compareCSVs(String file1PathName, String file2PathName, String file3PathName, Boolean ignoreDataRunDateStamp)  throws FileNotFoundException, IOException {
        List<SummaryGLLineDTO> al1=new ArrayList<SummaryGLLineDTO>();
        List<SummaryGLLineDTO> al2=new ArrayList<SummaryGLLineDTO>();

        BufferedReader CSVFile1 = new BufferedReader(new FileReader(file1PathName));
        String dataRow1 = CSVFile1.readLine(); //this is the header
        dataRow1 = CSVFile1.readLine(); //this is the first line of data
        while (dataRow1 != null)
        {
        	String[] dataArray1 = dataRow1.split(",");
            al1.add(ObjectToDtoFacade.convertToDtoFromCSVLine(dataArray1, ignoreDataRunDateStamp));
            dataRow1 = CSVFile1.readLine(); // Read next line of data.
        }

         CSVFile1.close();

        BufferedReader CSVFile2 = new BufferedReader(new FileReader(file2PathName));
        String dataRow2 = CSVFile2.readLine(); //this is the header
        dataRow2 = CSVFile2.readLine(); //this is the first line of data
        while (dataRow2 != null)
        {
        	String[] dataArray2 = dataRow2.split(",");
        	al2.add(ObjectToDtoFacade.convertToDtoFromCSVLine(dataArray2, ignoreDataRunDateStamp));
            dataRow2 = CSVFile2.readLine(); // Read next line of data.
        }
         CSVFile2.close();
         
         logger.debug("Size of al1 before remove: " + al1.size());
         logger.debug("Size of al2 before remove: " + al2.size());
         
         Boolean returnVal = true;
         if (al1.size() != al2.size()) {
        	 returnVal = false;
         }
         
         for(SummaryGLLineDTO bs:al2)
         {
             al1.remove(bs);
         }
         
         logger.debug("Size of al1 after remove: " + al1.size());
         logger.debug("Size of al2 after remove: " + al2.size());
         
         if (al1.size() != 0) {
        	 returnVal = false;
         }
         
         toSummaryGLLineCSVFile(al1, file3PathName);

         return returnVal;
    }
	
	static public String escapeSpecialCharacters(String data) {
	    String escapedData = data.replaceAll("\\R", " ");
	    escapedData = data.replaceAll("\\n", " ");
	    escapedData = data.replaceAll("\\t", " ");
	    escapedData = data.replaceAll(",", " ");
	    if (data.contains("\"") || data.contains("'")) {
	        data = data.replace("\"", "\"\"");
	        escapedData = "\"" + data + "\"";
	    }
	    return escapedData;
	}

}
