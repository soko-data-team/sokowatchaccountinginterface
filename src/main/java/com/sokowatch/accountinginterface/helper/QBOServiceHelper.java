package com.sokowatch.accountinginterface.helper;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.ReportService;
import com.intuit.ipp.util.Config;

@Component
public class QBOServiceHelper {
	
	@Autowired
	ConfigServiceImpl configServiceImpl;
	//private static final Logger logger = Logger.getLogger(QBOServiceHelper.class);

	public DataService getDataService(String realmId, String accessToken) throws FMSException, IOException {
		
    	Context context = prepareContext(realmId, accessToken);
		// create dataservice
		return new DataService(context);
	}

	private Context prepareContext(String realmId, String accessToken) throws FMSException, IOException {
		String url = configServiceImpl.getConfigValue("IntuitAccountingAPIHost") + "/v3/company";

		Config.setProperty(Config.BASE_URL_QBO, url);
		//create oauth object
		OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
		//create context
		Context context = new Context(oauth, ServiceType.QBO, realmId);
		return context;
	}
	
	public ReportService getReportService(String realmId, String accessToken) throws FMSException, IOException {
    	Context context = prepareContext(realmId, accessToken);
		// create reportservice
		return new ReportService(context);
	}
	
	/**
	 * This method is an override with duplication of the QBOServiceHelper
	 * to pass in a different minor version.  This is due to an invoice update
	 * failing with the discount line for minor version "23" (SDK default), hence
	 * overriding with the minor version "4"
	 *
	 * @param realmId
	 * @param accessToken
	 * @return
	 * @throws FMSException
	 * @throws IOException 
	 */
	public DataService getDataService(String realmId, String accessToken, String minorVersion) throws FMSException, IOException {
		Context context = prepareContext(realmId, accessToken);
		context.setMinorVersion(minorVersion);
		// create dataservice
		return new DataService(context);
	}
}
