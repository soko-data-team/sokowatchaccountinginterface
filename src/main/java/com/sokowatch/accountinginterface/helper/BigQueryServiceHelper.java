package com.sokowatch.accountinginterface.helper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;

@Component
public class BigQueryServiceHelper {
	@Value( "${ConfigFileLocation}" )
	private String configFileLocation;
	@Value( "${BigQueryCredentialsFileName}" )
	private String bigQueryCredentialsFileName;
	
	public BigQuery getDataService() throws FileNotFoundException, IOException {
		String bigQueryCredentialsFileLocation = configFileLocation + bigQueryCredentialsFileName;
		return BigQueryOptions.newBuilder().setCredentials(ServiceAccountCredentials
				.fromStream(new FileInputStream(bigQueryCredentialsFileLocation))
	            ).build().getService();
	}
}
