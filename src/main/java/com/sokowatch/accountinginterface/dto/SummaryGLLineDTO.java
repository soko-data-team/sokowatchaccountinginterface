package com.sokowatch.accountinginterface.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Objects;

import com.sokowatch.accountinginterface.enums.DataVersion;

//Note, if you change the fields in this class, you need to modify the ObjectToDto class
public class SummaryGLLineDTO {
	private DataVersion dataVersion;
	private String dataRunDateStamp = "";
	private String companyName="";
	private String accountNum="";
	private String accountName = "";
	private Boolean isCurrencyValue = true;
	private Currency accountCurrency; //this is the underlying currency of the account in the accounting system
	private Currency companyCurrency; //this the home currency/reporting currency of the entity to which the account belongs
	private String accountClassification = "";
	private String date="";
	private String accountingClass="";
	private String accountingLocation="";
	private BigDecimal amount = BigDecimal.ZERO;
	private BigDecimal closingBalance = BigDecimal.ZERO;
	
	public SummaryGLLineDTO() {
		super();
	}
	
	public SummaryGLLineDTO(DataVersion dataVersion,
			LocalDateTime dataRunDateStamp,
			String companyName, 
			String accountNum, 
			String accountName,
			Boolean isCurrencyValue,
			Currency accountCurrency,
			Currency companyCurrency,
			String accountClassification,
			LocalDate date, 
			String accountingClass, 
			String accountingLocation,
			BigDecimal amount, 
			BigDecimal closingBalance) {
		super();
		this.dataVersion = dataVersion;
		this.dataRunDateStamp = dataRunDateStamp.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
		this.companyName = companyName;
		this.accountNum = accountNum;
		this.accountName = accountName;
		this.isCurrencyValue = isCurrencyValue;
		this.accountCurrency = accountCurrency;
		this.companyCurrency = companyCurrency;
		this.accountClassification = accountClassification;
		this.date = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
		this.accountingClass = accountingClass;
		this.accountingLocation = accountingLocation;
		this.amount = amount;
		this.closingBalance = closingBalance;
	}
	
	public String getAccountNum() {
		return accountNum;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	public Boolean getIsCurrencyValue() {
		return isCurrencyValue;
	}

	public void setIsCurrencyValue(Boolean isCurrencyValue) {
		this.isCurrencyValue = isCurrencyValue;
	}
	
	public Currency getAccountCurrency() {
		return accountCurrency;
	}

	public void setAccountCurrency(Currency accountCurrency) {
		this.accountCurrency = accountCurrency;
	}

	public Currency getCompanyCurrency() {
		return companyCurrency;
	}

	public void setCompanyCurrency(Currency companyCurrency) {
		this.companyCurrency = companyCurrency;
	}

	public String getAccountClassification() {
		return accountClassification;
	}

	public void setAccountClassification(String accountClassification) {
		this.accountClassification = accountClassification;
	}

	public String getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		//this version is if it is called with a LocalDate
		this.date = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
	}
	
	public void setDate(String dateStr) {
		//this version is if it is called with a string
		this.date = dateStr;
	}

	public String getAccountingClass() {
		return accountingClass;
	}

	public void setAccountingClass(String accountingClass) {
		this.accountingClass = accountingClass;
	}

	public String getAccountingLocation() {
		return accountingLocation;
	}

	public void setAccountingLocation(String accountingLocation) {
		this.accountingLocation = accountingLocation;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	
	public DataVersion getDataVersion() {
		return dataVersion;
	}

	public void setDataVersion(DataVersion dataVersion) {
		this.dataVersion = dataVersion;
	}

	public String getDataRunDateStamp() {
		return dataRunDateStamp;
	}
	
	public void setDataRunDateStamp(LocalDateTime date) {
		//this version is if it is called with a LocalDate
		this.dataRunDateStamp = date.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
	}
	
	public void setDataRunDateStamp(String dateStr) {
		//this version is if it is called with a string
		this.dataRunDateStamp = dateStr;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataVersion, dataRunDateStamp, accountNum, accountName, isCurrencyValue, accountCurrency, companyCurrency, accountClassification, accountingClass, accountingLocation, amount, closingBalance, date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SummaryGLLineDTO other = (SummaryGLLineDTO) obj;
		return Objects.equals(dataVersion, other.dataVersion)
				&& Objects.equals(dataRunDateStamp, other.dataRunDateStamp)
				&& Objects.equals(accountNum, other.accountNum)
				&& Objects.equals(accountName, other.accountName) 
				&& Objects.equals(isCurrencyValue, other.isCurrencyValue) 
				&& Objects.equals(accountCurrency, other.accountCurrency)
				&& Objects.equals(companyCurrency, other.companyCurrency)
				&& Objects.equals(accountClassification, other.accountClassification)
				&& Objects.equals(accountingClass, other.accountingClass)
				&& Objects.equals(accountingLocation, other.accountingLocation) 
				&& Objects.equals(amount, other.amount)
				&& Objects.equals(closingBalance, other.closingBalance) 
				&& Objects.equals(date, other.date);
	}

	@Override
	public String toString() {
		return "SummaryGLLineDTO [dataVersion=" + dataVersion + ", dataRunDateStamp=" + dataRunDateStamp
				+ ", companyName=" + companyName + ", isCurrencyValue=" + isCurrencyValue + ", accountNum=" + accountNum + ", accountName=" + accountName
				+ ", accountCurrency=" + accountCurrency + ", companyCurrency=" + companyCurrency
				+ ", accountClassification=" + accountClassification + ", date=" + date + ", accountingClass="
				+ accountingClass + ", accountingLocation=" + accountingLocation + ", amount=" + amount
				+ ", closingBalance=" + closingBalance + "]";
	}
}
