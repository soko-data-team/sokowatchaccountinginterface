package com.sokowatch.accountinginterface.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Currency;

import com.sokowatch.accountinginterface.enums.DataVersion;
import com.sokowatch.accountinginterface.model.AccountClassLocation;
import com.sokowatch.accountinginterface.model.LedgerEntry;

public class ObjectToDtoFacade {
		
	public static SummaryGLLineDTO convertToDto(DataVersion dataVersion, LocalDateTime dataRunDateStamp, LocalDate date, AccountClassLocation accountClassLocation) {
		LocalDate actualEndOfMonth = LocalDate.of(date.getYear(), date.getMonth(), date.lengthOfMonth()); //set date to last day of month in case a non-end-of-month day is passed
		
		return new SummaryGLLineDTO(dataVersion,
				dataRunDateStamp,
				accountClassLocation.getCompanyName(),
				accountClassLocation.getAccountingPackageAccount().getNum(), 
				accountClassLocation.getAccountingPackageAccount().getName(),
				accountClassLocation.getAccountingPackageAccount().getIsCurrencyValue(),
				accountClassLocation.getAccountingPackageAccount().getCurrency(),
				accountClassLocation.getCompanyCurrency(),
				accountClassLocation.getAccountingPackageAccount().getClassification(),
				actualEndOfMonth, 
				accountClassLocation.getClassTag(),
				accountClassLocation.getLocationTag(), 
				accountClassLocation.getTotalAmountForMonthYear(date), 
				accountClassLocation.getClosingBalanceForMonthYear(date));
	}
	
	public static SummaryGLLineDTO convertToDtoFromCSVLine(String[] csvRow, Boolean ignoreDataRunDateStamp) {
		SummaryGLLineDTO dto = new SummaryGLLineDTO();
		dto.setDataVersion(DataVersion.valueOf(csvRow[0]));
		if (ignoreDataRunDateStamp) {
			dto.setDataRunDateStamp(LocalDateTime.now());
		}
		else {
			dto.setDataRunDateStamp(LocalDateTime.parse(csvRow[1], DateTimeFormatter.ISO_LOCAL_DATE_TIME));
		}
		dto.setCompanyName(csvRow[2]);
		dto.setDate(LocalDate.parse(csvRow[3], DateTimeFormatter.ISO_LOCAL_DATE));
		dto.setAccountNum(csvRow[4]);
		dto.setAccountName(csvRow[5]);
		dto.setIsCurrencyValue(Boolean.valueOf(csvRow[6]));
		dto.setAccountCurrency(Currency.getInstance(csvRow[7]));
		dto.setCompanyCurrency(Currency.getInstance(csvRow[8]));
		dto.setAccountClassification(csvRow[9]);
		dto.setAccountingClass(csvRow[10]);
		dto.setAccountingLocation(csvRow[11]);
		dto.setAmount(new BigDecimal(csvRow[12]));
		dto.setClosingBalance(new BigDecimal(csvRow[13]));
		
		return dto;
	}

	public static LedgerEntryDTO convertToDto(LedgerEntry ledgerEntry) {
		LedgerEntryDTO ledgerEntryDTO = new LedgerEntryDTO();
		ledgerEntryDTO.setAccountClassification(ledgerEntry.getAccountClassLocation().getAccountingPackageAccount().getClassification());
		ledgerEntryDTO.setAccountSubType(ledgerEntry.getAccountClassLocation().getAccountingPackageAccount().getAccountSubType());
		ledgerEntryDTO.setAccountingClass(ledgerEntry.getAccountClassLocation().getClassTag());
		ledgerEntryDTO.setAccountingLocation(ledgerEntry.getAccountClassLocation().getLocationTag());
		ledgerEntryDTO.setAccountName(ledgerEntry.getAccountClassLocation().getAccountingPackageAccount().getName());
		ledgerEntryDTO.setAccountNum(ledgerEntry.getAccountClassLocation().getAccountingPackageAccount().getNum());
		ledgerEntryDTO.setAmount(ledgerEntry.getAmount());
		ledgerEntryDTO.setCompanyName(ledgerEntry.getAccountClassLocation().getCompanyName());
		ledgerEntryDTO.setDataRunDateStamp(LocalDate.now().toString());
		ledgerEntryDTO.setDate(ledgerEntry.getDate().toString());
		ledgerEntryDTO.setSuspectedErrorHint(ledgerEntry.getSuspectedErrorHint());
		ledgerEntryDTO.setTransactionType(ledgerEntry.getTransactionType());
		ledgerEntryDTO.setDocNumber(ledgerEntry.getDocNumber());
		ledgerEntryDTO.setCustomerName(ledgerEntry.getCustomerName());
		ledgerEntryDTO.setApPaidStatus(ledgerEntry.getApPaidStatus());
		ledgerEntryDTO.setArPaidStatus(ledgerEntry.getArPaidStatus());
		ledgerEntryDTO.setClearedStatus(ledgerEntry.getClearedStatus());
		ledgerEntryDTO.setProductOrServiceName(ledgerEntry.getProductOServiceName());
		ledgerEntryDTO.setMemo(ledgerEntry.getMemo());
		ledgerEntryDTO.setName(ledgerEntry.getName());
		ledgerEntryDTO.setSplitAccount(ledgerEntry.getSplitAccount());
		ledgerEntryDTO.setSupplierName(ledgerEntry.getSupplierName());
		return ledgerEntryDTO;
	}
}
