package com.sokowatch.accountinginterface.dto;

import java.util.HashMap;
import java.util.Map;

public class ObjectToRowContentFacade {
	
	public static Map<String, Object> convertSummaryGLLineDTOToMap(SummaryGLLineDTO dto, String dataRunId) {
		Map<String, Object> rowContent = new HashMap<>();
		rowContent.put("DataVersion", dto.getDataVersion().toString());
		rowContent.put("DataRunDateStamp", dto.getDataRunDateStamp());
		rowContent.put("CompanyName", dto.getCompanyName());
		rowContent.put("Date", dto.getDate());
		rowContent.put("MetricID", dto.getAccountNum());
		rowContent.put("MetricName", dto.getAccountName());
		rowContent.put("IsCurrencyValue", dto.getIsCurrencyValue());
		rowContent.put("MetricCurrency", dto.getAccountCurrency().toString());
		rowContent.put("CompanyCurrency", dto.getCompanyCurrency().toString());
		rowContent.put("Classification", dto.getAccountClassification());
		rowContent.put("ClassTag", dto.getAccountingClass());
		rowContent.put("LocationTag", dto.getAccountingLocation());
		rowContent.put("Amount", dto.getAmount());
		rowContent.put("ClosingBalance", dto.getClosingBalance());
		rowContent.put("DataRunID", dataRunId);
		
		return rowContent;
	}

}
