package com.sokowatch.accountinginterface.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class LedgerEntryDTO {
	private String dataRunDateStamp = "";
	private String companyName="";
	private String accountNum="";
	private String accountName = "";
	private String accountClassification = "";
	private String accountSubType = "";
	private String date = "";
	private String accountingClass = "";
	private String accountingLocation = "";
	private BigDecimal amount = BigDecimal.ZERO;
	private String transactionType = "";
	private String docNumber = "";
	private String customerName = "";
	private String apPaidStatus = "";
	private String arPaidStatus = "";
	private String clearedStatus = "";
	private String productOrServiceName = "";
	private String memo = "";
	private String name = "";
	private String splitAccount = "";
	private String supplierName = "";
	private String suspectedErrorHint = "";
	
	public LedgerEntryDTO() {
		super();
	}

	/*public LedgerEntryDTO(String dataRunDateStamp, String companyName, String accountNum, String accountName,
			String accountClassification, String date, String accountingClass, String accountingLocation,
			BigDecimal amount, String suspectedErrorHint) {
		super();
		this.dataRunDateStamp = dataRunDateStamp;
		this.companyName = companyName;
		this.accountNum = accountNum;
		this.accountName = accountName;
		this.accountClassification = accountClassification;
		this.date = date;
		this.accountingClass = accountingClass;
		this.accountingLocation = accountingLocation;
		this.amount = amount;
		this.suspectedErrorHint = suspectedErrorHint;
	}
	*/

	public String getDataRunDateStamp() {
		return dataRunDateStamp;
	}

	public void setDataRunDateStamp(String dataRunDateStamp) {
		this.dataRunDateStamp = dataRunDateStamp;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAccountNum() {
		return accountNum;
	}

	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountClassification() {
		return accountClassification;
	}

	public void setAccountClassification(String accountClassification) {
		this.accountClassification = accountClassification;
	}
	
	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAccountingClass() {
		return accountingClass;
	}

	public void setAccountingClass(String accountingClass) {
		this.accountingClass = accountingClass;
	}

	public String getAccountingLocation() {
		return accountingLocation;
	}

	public void setAccountingLocation(String accountingLocation) {
		this.accountingLocation = accountingLocation;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSuspectedErrorHint() {
		return suspectedErrorHint;
	}

	public void setSuspectedErrorHint(String suspectedErrorHint) {
		this.suspectedErrorHint = suspectedErrorHint;
	}
	

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getApPaidStatus() {
		return apPaidStatus;
	}

	public void setApPaidStatus(String apPaidStatus) {
		this.apPaidStatus = apPaidStatus;
	}

	public String getArPaidStatus() {
		return arPaidStatus;
	}

	public void setArPaidStatus(String arPaidStatus) {
		this.arPaidStatus = arPaidStatus;
	}

	public String getClearedStatus() {
		return clearedStatus;
	}

	public void setClearedStatus(String clearedStatus) {
		this.clearedStatus = clearedStatus;
	}

	public String getProductOrServiceName() {
		return productOrServiceName;
	}

	public void setProductOrServiceName(String productOrServiceName) {
		this.productOrServiceName = productOrServiceName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSplitAccount() {
		return splitAccount;
	}

	public void setSplitAccount(String splitAccount) {
		this.splitAccount = splitAccount;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountClassification, accountSubType, accountName, accountNum, accountingClass, accountingLocation, amount,
				companyName, dataRunDateStamp, date, suspectedErrorHint);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LedgerEntryDTO other = (LedgerEntryDTO) obj;
		return Objects.equals(accountClassification, other.accountClassification)
				&& Objects.equals(accountSubType, other.accountSubType)
				&& Objects.equals(accountName, other.accountName) && Objects.equals(accountNum, other.accountNum)
				&& Objects.equals(accountingClass, other.accountingClass)
				&& Objects.equals(accountingLocation, other.accountingLocation) && Objects.equals(amount, other.amount)
				&& Objects.equals(companyName, other.companyName)
				&& Objects.equals(dataRunDateStamp, other.dataRunDateStamp) && Objects.equals(date, other.date)
				&& Objects.equals(suspectedErrorHint, other.suspectedErrorHint);
	}

	@Override
	public String toString() {
		return "LedgerEntryDTO [dataRunDateStamp=" + dataRunDateStamp + ", companyName=" + companyName + ", accountNum="
				+ accountNum + ", accountName=" + accountName + ", accountClassification=" + accountClassification
				+ ", date=" + date + ", accountingClass=" + accountingClass + ", accountingLocation="
				+ accountingLocation + ", amount=" + amount + ", suspectedErrorHint=" + suspectedErrorHint + "]";
	}
}
