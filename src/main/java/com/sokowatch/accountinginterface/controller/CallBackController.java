package com.sokowatch.accountinginterface.controller;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.sokowatch.accountinginterface.client.OAuth2PlatformClientFactory;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;
import com.sokowatch.accountinginterface.service.token.AccessTokenServiceImpl;
import com.sokowatch.accountinginterface.service.token.RefreshTokenServiceImpl;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.OAuthException;

@Controller
public class CallBackController {
	@Autowired
	OAuth2PlatformClientFactory factory;
	@Autowired
	AccessTokenServiceImpl accessTokenServiceImpl;
	@Autowired
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	@Autowired
	ConfigServiceImpl configServiceImpl;

    private static final Logger logger = Logger.getLogger(CallBackController.class);
    
    /**
     *  This is the redirect handler you configure in your app on developer.intuit.com
     *  The Authorization code has a short lifetime.
     *  Hence Unless a user action is quick and mandatory, proceed to exchange the Authorization Code for
     *  BearerToken
     *      
     * @param auth_code
     * @param state
     * @param realmId
     * @param session
     * @return
     * @throws Exception 
     */
    @RequestMapping("/oauth2redirect")
    public String callBackFromOAuth(@RequestParam("code") String authCode, @RequestParam("state") String state, @RequestParam(value = "realmId", required = false) String realmId, HttpSession session) throws Exception {   
        logger.info("inside oauth2redirect"  );
        try {
	        String csrfToken = (String) session.getAttribute("csrfToken");
	        if (csrfToken.equals(state)) {
	            OAuth2PlatformClient client  = factory.getOAuth2PlatformClient();
	            String redirectUri = configServiceImpl.getConfigValue("QBOOAuth2AppRedirectUri");
	            logger.info("inside oauth2redirect -- redirectUri " + redirectUri  );
	            
	            BearerTokenResponse bearerTokenResponse = client.retrieveBearerTokens(authCode, redirectUri);
				 
	            // Update Data store here with user's AccessToken and RefreshToken along with the realmId
	            //session.setAttribute("access_token", bearerTokenResponse.getAccessToken());
	            //session.setAttribute("refresh_token", bearerTokenResponse.getRefreshToken());
	            //session.setAttribute("realmId", realmId);
	            
	            accessTokenServiceImpl.createTokenForRealmId(realmId, bearerTokenResponse.getAccessToken());
	            refreshTokenServiceImpl.createTokenForRealmId(realmId, bearerTokenResponse.getRefreshToken());
	            
	            return "connected";
	        }
	        logger.info("csrf token mismatch " );
        } catch (OAuthException e) {
        	logger.error("Exception in callback handler ", e);
		} 
        return null;
    }
}
