package com.sokowatch.accountinginterface.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import com.sokowatch.accountinginterface.client.OAuth2PlatformClientFactory;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;
import com.intuit.oauth2.config.OAuth2Config;
import com.intuit.oauth2.config.Scope;
import com.intuit.oauth2.exception.InvalidRequestException;

@Controller
public class HomeController {
	
	private static final Logger logger = Logger.getLogger(HomeController.class);
	
	@Autowired
	OAuth2PlatformClientFactory factory;
	@Autowired
	ConfigServiceImpl configServiceImpl;
	    
	@RequestMapping("/")
	public String home() {
		return "home";
	}
	
	@RequestMapping("/home")
	public String home2() {
		return home();
	}
	
	@RequestMapping("/connected")
	public String connected() {
		return "connected";
	}
	
	//@RequestMapping("/oauth_login")
	//public String oauth_login() {
	//	return "oauth_login";
	//}
	
	/**
	 * Controller mapping for connectToQuickbooks button
	 * @return
	 * @throws IOException 
	 */
	@RequestMapping("/connectToQuickbooks")
	public View connectToQuickbooks(HttpSession session) throws IOException {
		logger.info("inside connectToQuickbooks");
		return doOauth2Redirect(session);
	}
	
	private View doOauth2Redirect(HttpSession session) throws IOException {
		factory.setupOauth2Config(session);
		OAuth2Config oauth2Config = factory.getOAuth2Config();
		
		String redirectUri = configServiceImpl.getConfigValue("QBOOAuth2AppRedirectUri"); 
		
		String csrf = oauth2Config.generateCSRFToken();
		session.setAttribute("csrfToken", csrf);
		try {
			List<Scope> scopes = new ArrayList<Scope>();
			scopes.add(Scope.Accounting);
			return new RedirectView(oauth2Config.prepareUrl(scopes, redirectUri, csrf), true, true, false);
		} catch (InvalidRequestException e) {
			logger.error("Exception calling connectToQuickbooks for company" + session.getAttribute("company"), e);
		}
		
		return null;
	}
}
