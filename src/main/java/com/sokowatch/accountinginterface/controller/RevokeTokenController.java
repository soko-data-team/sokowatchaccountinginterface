package com.sokowatch.accountinginterface.controller;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.sokowatch.accountinginterface.client.OAuth2PlatformClientFactory;
import com.sokowatch.accountinginterface.service.token.RefreshTokenServiceImpl;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.data.PlatformResponse;

@RestController
@RequestMapping("/api/accountingsystem")
public class RevokeTokenController {
	
	@Autowired
	OAuth2PlatformClientFactory factory;
	@Autowired
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	
	private static final Logger logger = Logger.getLogger(RevokeTokenController.class);
	
    /**
     * Call to revoke tokens 
     * 
     * @param session
     * @return
     */
	@ResponseBody
    @RequestMapping("/revokeToken/{realmID}")
    public String revokeToken(@PathVariable String realmId) {
		
    	String failureMsg="Failed";
    	      
        try {
        	OAuth2PlatformClient client  = factory.getOAuth2PlatformClient();
        	String refreshToken = refreshTokenServiceImpl.getTokenForRealmId(realmId);
        	
        	PlatformResponse response  = client.revokeToken(refreshToken);
            logger.info("raw result for revoke token request= " + response.getStatus());
            return new JSONObject().put("response", "Revoke successful").toString();
        }
        catch (Exception ex) {
        	logger.error("Exception while calling revokeToken ", ex);
        	return new JSONObject().put("response",failureMsg).toString();
        }    
        
    }
}
