package com.sokowatch.accountinginterface.controller;

import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.sokowatch.accountinginterface.client.OAuth2PlatformClientFactory;
import com.sokowatch.accountinginterface.service.token.AccessTokenServiceImpl;
import com.sokowatch.accountinginterface.service.token.RefreshTokenServiceImpl;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.data.BearerTokenResponse;

@RestController
@RequestMapping("/api/accountingsystem")
public class RefreshTokenController {
	
	@Autowired
	OAuth2PlatformClientFactory factory;
	@Autowired
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	@Autowired
	AccessTokenServiceImpl accessTokenServiceImpl;
	
	private static final Logger logger = Logger.getLogger(RefreshTokenController.class);
	
    /**
     * Call to refresh tokens 
     * 
     * @param session
     * @return
     */
	@ResponseBody
	@RequestMapping("/refreshToken/{realmID}")
	public String refreshToken(@PathVariable String realmId, HttpSession session) {

		String failureMsg="Failed";

		try {
			factory.setupOauth2Config(session);
			OAuth2PlatformClient client  = factory.getOAuth2PlatformClient();
			String refreshToken = refreshTokenServiceImpl.getTokenForRealmId(realmId);

			BearerTokenResponse bearerTokenResponse = client.refreshToken(refreshToken);

			accessTokenServiceImpl.createTokenForRealmId(realmId, bearerTokenResponse.getAccessToken());
			refreshTokenServiceImpl.createTokenForRealmId(realmId, bearerTokenResponse.getRefreshToken());

			//session.setAttribute("access_token", bearerTokenResponse.getAccessToken());
			//session.setAttribute("refresh_token", bearerTokenResponse.getRefreshToken());
			/*String jsonString = new JSONObject()
                    .put("access_token", bearerTokenResponse.getAccessToken())
                    .put("refresh_token", bearerTokenResponse.getRefreshToken()).toString();
            return jsonString;
			 */

			return new JSONObject().put("response", "Refresh successful").toString();
		}
		catch (Exception ex) {
			logger.error("Exception while calling refreshToken ", ex);
			return new JSONObject().put("response",failureMsg).toString();
		}    
	}
}
