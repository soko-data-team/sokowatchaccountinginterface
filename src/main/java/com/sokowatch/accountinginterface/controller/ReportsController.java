package com.sokowatch.accountinginterface.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.bigquery.BigQueryException;
import com.sokowatch.accountinginterface.client.OAuth2PlatformClientFactory;
import com.sokowatch.accountinginterface.dto.LedgerEntryDTO;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;
import com.sokowatch.accountinginterface.helper.CSVHelper;
import com.sokowatch.accountinginterface.model.collection.AccountClassLocations;
import com.sokowatch.accountinginterface.service.bigquery.Accounts_Entity_Account_Class_Location_ByMonthService;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;
import com.sokowatch.accountinginterface.service.quickbooks.CompanyInfoService;
import com.sokowatch.accountinginterface.service.quickbooks.GeneralLedgerService;
import com.sokowatch.accountinginterface.service.token.AccessTokenServiceImpl;
import com.sokowatch.accountinginterface.service.token.RefreshTokenServiceImpl;
import com.intuit.ipp.data.Error;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.exception.InvalidTokenException;

@Controller
@RequestMapping("/api/accountingsystem/reports")
public class ReportsController {

	@Autowired
	GeneralLedgerService generalLedgerService;
	@Autowired
	CompanyInfoService companyInfoService;
	@Autowired
	Accounts_Entity_Account_Class_Location_ByMonthService accounts_Entity_Account_Class_Location_ByMonthService;
	@Autowired
	OAuth2PlatformClientFactory factory;
	@Autowired
	Accounts_Entity_Account_Class_Location_ByMonthService bigQueryService;
	@Autowired
	AccessTokenServiceImpl accessTokenServiceImpl;
	@Autowired
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	@Autowired
	RefreshTokenController refreshTokenController;
	@Autowired
	ConfigServiceImpl configServiceImpl;

	private static final Logger logger = Logger.getLogger(ReportsController.class);
	
	/**
	 * Extracts ledger for RealmId and period, transforms and loads into the warehouse
	 * @param toDate
	 * @param realmId
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@GetMapping("/ledger/{toDateFromWeb}/{realmId}")
	public String callLedgerConcept(@PathVariable String toDateFromWeb, @PathVariable String realmId, HttpSession session) {
		factory.setupOauth2Config(session);
		
		String returnString = "";
		
		try {
			//Measure the time taken
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();

			/*** Do checks */
			if (StringUtils.isEmpty(realmId)) {
				return new JSONObject()
						.put("response", "No realm ID.  QBO calls only work if the accounting scope was passed!")
						.toString();
			}
			
			String companyStartDate = configServiceImpl.getConfigValue("Company.StartDate."+realmId);
			if (StringUtils.isEmpty(companyStartDate)) {
				return new JSONObject()
						.put("response", "Start Date is not set for the company. Please ensure the start date for the company is set in the application.config (speak to the system owner about this)")
						.toString();
			}

			if (StringUtils.isEmpty(toDateFromWeb)) {
				return new JSONObject()
						.put("response", "Please choose 'To Date' and resubmit")
						.toString();
			}
			/*** End checks */
			
			//Refresh tokens
			refreshTokenController.refreshToken(realmId, session);
			//Get the latest AccessToken
			String accessToken = accessTokenServiceImpl.getTokenForRealmId(realmId);
			
			LocalDate fromDate = LocalDate.parse(companyStartDate, DateTimeFormatter.ISO_LOCAL_DATE);
			LocalDate toDate = LocalDate.parse(toDateFromWeb, DateTimeFormatter.ISO_LOCAL_DATE);//LocalDate.of(2020,7, 31);
			//force toDate to be the last day of the month
			LocalDate toDateEndOfMonth = LocalDate.of(toDate.getYear(), toDate.getMonth(), toDate.lengthOfMonth());

			AccountClassLocations accountClassLocationsForWholeTimePeriod = generalLedgerService.getAccountClassLocationsForYearMonths(realmId, 
					accessToken, fromDate, toDateEndOfMonth);

			/*
			* Uncomment the below line if you want to output the full data set to Csv. Useful for testing
			List<SummaryGLLineDTO> dtos = this.getSummaryGLLineDTOsForPeriodAndOutputToCsv(this.getListOfYearMonthsBetweenTwoDates(fromDate, toDate),
					LocalDate.parse(factory.getPropertyValue("OutputFromDate")), accountClassLocationsForWholeTimePeriod);

			 */
			List<SummaryGLLineDTO> summaryGLLineDTOs = this.getSummaryGLLineDTOsForPeriodAndOutputToBigQuery(this.getListOfYearMonthsBetweenTwoDates(fromDate, toDate),
					LocalDate.parse(configServiceImpl.getConfigValue("OutputFromDate")), accountClassLocationsForWholeTimePeriod);

			List<LedgerEntryDTO> suspectedErrorLedgerEntryDTOs = this.getSuspectedErrorJournalDTOsForPeriodAndOutputToCSV(this.getListOfYearMonthsBetweenTwoDates(fromDate, toDate),
					LocalDate.parse(configServiceImpl.getConfigValue("OutputFromDate")), accountClassLocationsForWholeTimePeriod);

			stopWatch.stop();
			returnString ="Success loading QB data for " + companyInfoService.getCompanyInfo(realmId, accessToken).getCompanyName() + " for the period " + fromDate.toString() + " - " + toDateEndOfMonth.toString() + " with [" + suspectedErrorLedgerEntryDTOs.size() + "] suspected tagging Errors. Time taken was " + String.format("%.2f", stopWatch.getTotalTimeSeconds() / 60) + " minutes.";

		} catch (InvalidTokenException e) {
			logger.error("Invalid token: ", e);
			return new JSONObject().put("response", "InvalidToken - Refresh token and try again").toString();
		} catch (FMSException e) {
			List<Error> list = e.getErrorList();
			list.forEach(error -> logger.error("FMSException. Error while calling the API :: " + error.getMessage()));
			return new JSONObject().put("response", "Intuit Exception. Please check logs").toString();
		} catch (BigQueryException e) {
			logger.error("BigQueryException: ", e);
			return new JSONObject().put("response", "Failed. BigQuery error. Please check logs").toString();
		} catch (IOException e) {
			logger.error("Exception: ", e);
			return new JSONObject().put("response", "Failed. IO error: " + e.getMessage()).toString();
		}catch (Exception e) {
			logger.error("Exception: ", e);
			return new JSONObject().put("response", "Failed. General error: " + e.getMessage()).toString();
		}

		logger.debug(returnString);
		return createResponse(returnString);
	}
	
	/**
	 * Wipes clean the suspected errors file
	 * @param session
	 * @return
	 * @throws Exception 
	 */
	@ResponseBody
	@GetMapping("/clearsuspectederrorcsvfile")
	public String clearSuspectedErrorCSVFile(HttpSession session) {
		String returnString = "";
		try {
			String folder = configServiceImpl.getConfigValue("OutputFileLocation");
			String fileToDeleteFilePathName = folder+configServiceImpl.getConfigValue("SuspectedJournalErrorFileName");
			String templateFilePathName = folder+configServiceImpl.getConfigValue("SuspectedJournalErrorTemplateFileName");
			CSVHelper.replaceFileWithTemplateFile(fileToDeleteFilePathName, templateFilePathName);
			returnString = "Success";
		} catch (Exception e) {
			returnString = "Error refreshing SuspectedError File: " + e.getMessage();
		}
		return createResponse(returnString);
	}
	
	private List<SummaryGLLineDTO> getSummaryGLLineDTOsForPeriodAndOutputToBigQuery(List<LocalDate> yearMonthsCoveredByRun, LocalDate outputFromDate, AccountClassLocations accountClassLocationsForWholeTimePeriod) throws Exception {

		//iterate through the list of YearMonths and build the DTO's for each and add to the array of DTOs
		List<SummaryGLLineDTO> dtos = accountClassLocationsForWholeTimePeriod.toSummaryGLLineDTOsForPeriod(yearMonthsCoveredByRun, outputFromDate);

		logger.debug("Start outputting to BigQuery");
		accounts_Entity_Account_Class_Location_ByMonthService.uploadData(dtos);
		logger.debug("Done outputting to BigQuery");

		return dtos;
	}
	
	private List<SummaryGLLineDTO> getSummaryGLLineDTOsForPeriodAndOutputToCsv(List<LocalDate> yearMonthsCoveredByRun, LocalDate outputFromDate, AccountClassLocations accountClassLocationsForWholeTimePeriod) throws FMSException, IOException {
		//iterate through the list of YearMonths and build the DTO's for each and add to the array of DTOs
		List<SummaryGLLineDTO> dtos = accountClassLocationsForWholeTimePeriod.toSummaryGLLineDTOsForPeriod(yearMonthsCoveredByRun, outputFromDate);

		logger.debug("Start outputting to CSV");
		CSVHelper.toSummaryGLLineCSVFile(dtos, configServiceImpl.getConfigValue("OutputFileLocation")+configServiceImpl.getConfigValue("OutputFileName"));
		logger.debug("Done outputting to CSV");

		return dtos;
	}
	
	private List<LedgerEntryDTO> getSuspectedErrorJournalDTOsForPeriodAndOutputToCSV(List<LocalDate> yearMonthsCoveredByRun, LocalDate outputFromDate, AccountClassLocations accountClassLocationsForWholeTimePeriod) throws IOException {
		//iterate through the list of YearMonths and build the DTO's for each and add to the array of DTOs
		List<LedgerEntryDTO> dtos = accountClassLocationsForWholeTimePeriod.toSuspectedErrorJournalDTOsForPeriod(yearMonthsCoveredByRun);

		logger.debug("Start outputting Suspected Errors to CSV");
		CSVHelper.toJournalEntryCSVFile(dtos, configServiceImpl.getConfigValue("OutputFileLocation")+configServiceImpl.getConfigValue("SuspectedJournalErrorFileName"));
		logger.debug("Done outputting Suspected Errors to CSV");
		//Remember to just append to the existing file

		return dtos;
	}
	
	/**
	 * Map object to json string
	 * 
	 * @param entity
	 * @return
	 */
	private String createResponse(Object entity) {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString;
		try {
			jsonInString = mapper.writeValueAsString(entity);
		} catch (JsonProcessingException e) {
			return createErrorResponse(e);
		} catch (Exception e) {
			return createErrorResponse(e);
		}
		return jsonInString;
	}

	private String createErrorResponse(Exception e) {
		logger.error("Exception while calling QBO ", e);
		return new JSONObject().put("response", "Failed").toString();
	}
	
	private List<LocalDate> getListOfYearMonthsBetweenTwoDates(LocalDate fromDate, LocalDate toDate) {
		List<LocalDate> yearMonthsList = new ArrayList<LocalDate>();
		LocalDate arrayDate = LocalDate.of(fromDate.getYear(), fromDate.getMonth(), fromDate.lengthOfMonth());
		while(arrayDate.compareTo(toDate)<=0) {
			yearMonthsList.add(arrayDate);
			arrayDate = arrayDate.plusMonths(1L).with(TemporalAdjusters.lastDayOfMonth());
		}
		
		return yearMonthsList;
	}
}
