package com.sokowatch.accountinginterface.controller;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.sokowatch.accountinginterface.model.MailObject;
import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;
import com.sokowatch.accountinginterface.service.email.EmailService;

@RestController
@RequestMapping("/api/mail")
public class EmailController {
	
	@Autowired
    public EmailService emailService;
	@Autowired
	ConfigServiceImpl configServiceImpl;

    @ResponseBody
	@GetMapping("/sendAttachment/{toAddress}")
    public String sendAttachment(@PathVariable String toAddress, HttpSession session) throws MessagingException, IOException {
        MailObject mo = new MailObject();
        mo.setTo(toAddress);
        mo.setSubject("SuspectedErrors File");
        mo.setText("Latest SuspectedErrors File");
        return createMailWithAttachment(mo);
    }

    private String createMailWithAttachment(@ModelAttribute("mailObject") @Valid MailObject mailObject) throws MessagingException, IOException { //, Errors errors) throws MessagingException {
    	String attachmentPath = configServiceImpl.getConfigValue("OutputFileLocation") + configServiceImpl.getConfigValue("SuspectedJournalErrorFileName");
    	
    	emailService.sendMessageWithAttachment(
                mailObject.getTo(),
                mailObject.getSubject(),
                mailObject.getText(),
                attachmentPath
        );

        return "Sent";
    }
}
