package com.sokowatch.accountinginterface.enums;

public enum DataVersion {
	FORECAST,
	ACTUAL
}
