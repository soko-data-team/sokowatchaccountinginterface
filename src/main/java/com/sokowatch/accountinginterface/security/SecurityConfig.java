package com.sokowatch.accountinginterface.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	/*
	 * Note, the security is currently configured on https://console.developers.google.com/apis/credentials.  
	 * I have configured it that access is limited to people with a sokowatch email account
	 * on the OAuth consent screen by setting the user type to "internal" 
	 * (which implies the user has to log in to, to prove they are part of the Sokowatch domain) 
	 * Currently the security is not against any particular user though.  
	 * The setup mentioned above does the security and if you have logged into your sokowatch 
	 * account it let's you in. Which is fine for now as it is just reading data. If the functionality 
	 * is increased to included writing you will have to add some sort of role-based security, which will involve
	 * setting up users and roles 
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/", "/home", "/oauth_login", "/logout", "/loginFailure").permitAll()
		.antMatchers("/resources/**").permitAll()
		.antMatchers("/*.css").permitAll()
		.antMatchers("/*.js").permitAll()
		.antMatchers("/api/**").authenticated()
		.antMatchers("/connected*").authenticated()

		//.antMatchers("/oauth_login", "/loginFailure", "/logout", "/home", "/", "/static/*")
		//.antMatchers("/*").permitAll()
		//.anyRequest().authenticated()
		.and()
		.oauth2Login()
		.loginPage("/oauth_login")
		.authorizationEndpoint()
		.baseUri("/oauth2/authorize-client")
		.authorizationRequestRepository(authorizationRequestRepository())
		.and()
		.tokenEndpoint()
		.accessTokenResponseClient(accessTokenResponseClient())
		.and()
		.defaultSuccessUrl("/connected")
		.failureUrl("/loginFailure");

		http.logout()
		.logoutSuccessUrl("/");

		//http.requiresChannel()
		//.antMatchers("/login*").requiresSecure();
	}

	@Bean
	public AuthorizationRequestRepository<OAuth2AuthorizationRequest> authorizationRequestRepository() {
		return new HttpSessionOAuth2AuthorizationRequestRepository();
	}

	@Bean
	public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> accessTokenResponseClient() {
		DefaultAuthorizationCodeTokenResponseClient accessTokenResponseClient = new DefaultAuthorizationCodeTokenResponseClient();
		return accessTokenResponseClient;
	}
}
