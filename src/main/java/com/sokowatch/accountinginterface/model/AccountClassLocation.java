package com.sokowatch.accountinginterface.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class AccountClassLocation {
	private AccountingPackageAccount accountingPackageAccount;
	private String classTag;
	private String locationTag;
	private String companyName;
	private Currency companyCurrency; //this is the home currency of the entity in which the account resides
	private List<LedgerEntry> ledgerEntries = new ArrayList<LedgerEntry>();
	
	
	public AccountClassLocation() {
		super();
	}
	
	public BigDecimal getTotalAmountForMonthYear(LocalDate endOfMonth) {
		LocalDate actualEndOfMonth = LocalDate.of(endOfMonth.getYear(), endOfMonth.getMonth(), endOfMonth.lengthOfMonth()); //set endOfMonth to last day of month in case a non-end-of-month day is passed
		BigDecimal total = new BigDecimal(0);
		Iterator<LedgerEntry> ledgerEntryIterator = ledgerEntries.iterator();
		while(ledgerEntryIterator.hasNext()) {
			LedgerEntry ledgerEntry = ledgerEntryIterator.next();
			if (ledgerEntry.getDate().withDayOfMonth(ledgerEntry.getDate().lengthOfMonth()).equals(actualEndOfMonth)) {
				total = total.add(ledgerEntry.getAmount());
			}
		}
		return total;
	}
	
	public BigDecimal getClosingBalanceForMonthYear(LocalDate endOfMonth) {
		LocalDate actualEndOfMonth = LocalDate.of(endOfMonth.getYear(), endOfMonth.getMonth(), endOfMonth.lengthOfMonth()); //set endOfMonth to last day of month in case a non-end-of-month day is passed
		BigDecimal total = BigDecimal.ZERO;
		
		Iterator<LedgerEntry> ledgerEntryIterator = ledgerEntries.iterator();
		while(ledgerEntryIterator.hasNext()) {
			LedgerEntry ledgerEntry = ledgerEntryIterator.next();
			
			int dateCompare = actualEndOfMonth.compareTo(ledgerEntry.getDate().withDayOfMonth(ledgerEntry.getDate().lengthOfMonth()));
			//If dateCompare = 0, the dates are equal, if +ve last day of the month we are interested in is greater than the ledgerentry month and if negative than vice versa
			
			if (dateCompare >= 0) {
				total = total.add(ledgerEntry.getAmount());
			}
		}
		return total;
	}

	public AccountingPackageAccount getAccountingPackageAccount() {
		return accountingPackageAccount;
	}

	public void setAccountingPackageAccount(AccountingPackageAccount accountingPackageAccount) {
		this.accountingPackageAccount = accountingPackageAccount;
	}

	public String getClassTag() {
		return classTag;
	}

	public void setClassTag(String classTag) {
		if(classTag.isEmpty()) {
			this.classTag = "Not Set";
		}
		else {
			this.classTag = classTag;
		}
	}

	public String getLocationTag() {
		return locationTag;
	}

	public void setAccountingLocation(String locationTag) {
		if(locationTag.isEmpty()) {
			this.locationTag = "Not Set";
		}
		else { 
			this.locationTag = locationTag;
		}
	}
	
	public Currency getCompanyCurrency() {
		return companyCurrency;
	}

	public void setCompanyCurrency(Currency companyCurrency) {
		this.companyCurrency = companyCurrency;
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public List<LedgerEntry> getLedgerEntries() {
		return ledgerEntries;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountingPackageAccount, accountingPackageAccount, classTag, locationTag, companyCurrency, companyName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountClassLocation other = (AccountClassLocation) obj;
		return Objects.equals(accountingPackageAccount, other.accountingPackageAccount)
				&& Objects.equals(classTag, other.classTag)
				&& Objects.equals(locationTag, other.locationTag)
				&& Objects.equals(companyCurrency, other.companyCurrency)
				&& Objects.equals(companyName, other.companyName);
	}

	@Override
	public String toString() {
		return "AccountClassLocation [accountingPackageAccount=" + accountingPackageAccount + ", classTag="
				+ classTag + ", locationTag=" + locationTag + ", companyName=" + companyName
				+ ", companyCurrency=" + companyCurrency + ", ledgerEntries=" + ledgerEntries + "]";
	}
}
