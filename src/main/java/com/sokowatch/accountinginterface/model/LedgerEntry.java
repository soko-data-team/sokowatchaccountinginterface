package com.sokowatch.accountinginterface.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class LedgerEntry {
	private LocalDate date;
	private AccountClassLocation accountClassLocation;
	private BigDecimal amount;
	
	private String transactionType = "";
	private String docNumber = "";
	private String customerName = "";
	private String apPaidStatus = "";
	private String arPaidStatus = "";
	private String clearedStatus = "";
	private String productOrServiceName = "";
	private String memo = "";
	private String name = "";
	private String splitAccount = "";
	private String supplierName = "";
	
	public LedgerEntry() {
		super();
	}
	
	public String getSuspectedErrorHint() {
		//if(this.getDate().isBefore(LocalDate.of(2020, 9, 1))) { //only checking for journals post this date
		//	return "";
		//}
		String returnVal = "";

		if(this.getDate().isAfter(LocalDate.of(2020, 8, 31)) && !this.getTransactionType().equals("Transfer")) { //only checking for journals post this date

			if (this.getAmount().compareTo(BigDecimal.ZERO) == 0) { //if nothing has been posted to it in this period, then we don't worry about it
				returnVal = "";
			}

			if(hasDeprecatedClassTag()) {
				returnVal = "Deprecated Class Tag";
			}

			if(this.hasDeprecatedLocationTag()) {
				returnVal = "Deprecated Location Tag";
			}

			if(this.classOrLocationNotSet()) {
				returnVal = "Class and/or Location not set";
			}
		}
		return returnVal;
	}
	
	public Boolean hasSuspectedError() {
		String suspectedErrorHint = this.getSuspectedErrorHint();
		
		if (suspectedErrorHint.equals("")) {
			return false;
		}
		return true;
	}
	
	private boolean hasDeprecatedClassTag() {
		Boolean returnVal = false;
		switch(this.getAccountClassLocation().getClassTag()) {
		case "Arusha":
		case "Dar es Salaam East":
		case "Dar es Salaam West":
		case "Global":
		case "Kenya":
		case "Mwanza":
		case "Rwanda":
		case "Tanzania":
		case "Uganda":
		case "Eldoret":
		case "Mombasa":
		case "Nairobi East":
		case "Nairobi West":
		case "Nakuru":
		case "Kigali":
		case "Kigali:Emmanuel  Sengiyumva":
		case "Kigali:Etienne":
		case "Kigali:Jean Baptist":
		case "Kigali:Misago  Silivain":
		case "Kigali:Peter  Kayitankore":
		case "Kampala":
		case "Arusha (deleted)":
		case "Dar es Salaam East (deleted)":
		case "Dar es Salaam West (deleted)":
		case "Global (deleted)":
		case "Kenya (deleted)":
		case "Mwanza (deleted)":
		case "Rwanda (deleted)":
		case "Tanzania (deleted)":
		case "Uganda (deleted)":
		case "Eldoret (deleted)":
		case "Mombasa (deleted)":
		case "Nairobi East (deleted)":
		case "Nairobi West (deleted)":
		case "Nakuru (deleted)":
		case "Kigali (deleted)":
		case "Kigali (deleted):Emmanuel  Sengiyumva (deleted)":
		case "Kigali (deleted):Etienne (deleted)":
		case "Kigali (deleted):Jean Baptist (deleted)":
		case "Kigali (deleted):Misago  Silivain (deleted)":
		case "Kigali (deleted):Peter  Kayitankore (deleted)":
		case "Kampala (deleted)":
		    returnVal = true;
		    break;
		}
		
		return returnVal;
	}
	
	private boolean hasDeprecatedLocationTag() {
		Boolean returnVal = false;
		switch(this.getAccountClassLocation().getLocationTag()) {
		case "Kenya:Kisumu":
		case "Mwanzaar":
		case "Kigali:Jean Baptist":
		case "Kigali:Emmanuel  Sengiyumva":
		case "Kigali:Misago  Silivain":
		case "Kigali:Peter  Kayitankore":
		case "Kigali:Etienne":
		case "Kigali:Hitimana Bosco":
		case "04":
		case "04.":
		case "05":
		case "05.Eldoret05.":
		case "053":
		case "08. Credit":
		case "Phone financing":
		case "HQ":
		case "Lavington":
		case "Pipeline":
		case "Kenya (deleted):Kisumu (deleted)":
		case "Mwanzaar (deleted)":
		case "Kigali (deleted):Jean Baptist (deleted)":
		case "Kigali (deleted):Emmanuel  Sengiyumva (deleted)":
		case "Kigali (deleted):Misago  Silivain (deleted)":
		case "Kigali (deleted):Peter  Kayitankore (deleted)":
		case "Kigali (deleted):Etienne (deleted)":
		case "Kigali (deleted):Hitimana Bosco (deleted)":
		case "04 (deleted)":
		case "04. (deleted)":
		case "05 (deleted)":
		case "05.Eldoret05. (deleted)":
		case "053 (deleted)":
		case "08. Credit (deleted)":
		case "Phone financing (deleted)":
		case "HQ (deleted)":
		case "Lavington (deleted)":
		case "Pipeline (deleted)":
		    returnVal = true;
		    break;
		}
		
		return returnVal;
	}

	private Boolean classOrLocationNotSet() {
		Boolean returnVal = false;
		if(this.getAccountClassLocation().getClassTag().equals("Not Set")) {
			return true;
		}
		
		if(this.getAccountClassLocation().getClassTag().equals("Not Set")) {
			return true;
		}
		
		return returnVal;
	}
	
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public AccountClassLocation getAccountClassLocation() {
		return accountClassLocation;
	}
	public void setAccountClassLocation(AccountClassLocation accountClassLocation) {
		this.accountClassLocation = accountClassLocation;
		accountClassLocation.getLedgerEntries().add(this);
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getApPaidStatus() {
		return apPaidStatus;
	}

	public void setApPaidStatus(String apPaidStatus) {
		this.apPaidStatus = apPaidStatus;
	}

	public String getArPaidStatus() {
		return arPaidStatus;
	}

	public void setArPaidStatus(String arPaidStatus) {
		this.arPaidStatus = arPaidStatus;
	}

	public String getClearedStatus() {
		return clearedStatus;
	}

	public void setClearedStatus(String clearedStatus) {
		this.clearedStatus = clearedStatus;
	}

	public String getProductOServiceName() {
		return productOrServiceName;
	}

	public void setProductOrServiceName(String productOServiceName) {
		this.productOrServiceName = productOServiceName;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSplitAccount() {
		return splitAccount;
	}

	public void setSplitAccount(String splitAccount) {
		this.splitAccount = splitAccount;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountClassLocation, amount, date);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LedgerEntry other = (LedgerEntry) obj;
		return Objects.equals(accountClassLocation, other.accountClassLocation) && Objects.equals(amount, other.amount)
				&& Objects.equals(date, other.date);
	}

	@Override
	public String toString() {
		return "LedgerEntry [date=" + date + ", accountClassLocation=" + accountClassLocation + ", amount=" + amount + "]";
	}
}
