package com.sokowatch.accountinginterface.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Objects;

public class AccountingPackageAccount {
	private String id;
	private String num;
	private String name;
	private Boolean isCurrencyValue;
	private String classification;
	private String accountSubType;
	private String status;
	private Currency currency; //this is the currency of the account.  Not to be confused with the reporting currency of the entity
	private BigDecimal currentBalance = BigDecimal.ZERO;
	private BigDecimal currentBalanceWithSubAccounts = BigDecimal.ZERO;
	
	public AccountingPackageAccount() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getIsCurrencyValue() {
		return isCurrencyValue;
	}

	public void setIsCurrencyValue(Boolean isCurrencyValue) {
		this.isCurrencyValue = isCurrencyValue;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}
	
	public String getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType) {
		this.accountSubType = accountSubType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
		
		if(status == null) {
			this.status = "";
		}
	}

	public BigDecimal getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(BigDecimal currentBalance) {
		this.currentBalance = currentBalance;
	}

	public BigDecimal getCurrentBalanceWitthSubAccounts() {
		return currentBalanceWithSubAccounts;
	}

	public void setCurrentBalanceWitthSubAccounts(BigDecimal currentBalanceWitthSubAccounts) {
		this.currentBalanceWithSubAccounts = currentBalanceWitthSubAccounts;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, num);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountingPackageAccount other = (AccountingPackageAccount) obj;
		return id == other.id && Objects.equals(num, other.num);
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", num=" + num + ", currency=" + currency + "]";
	}
}
