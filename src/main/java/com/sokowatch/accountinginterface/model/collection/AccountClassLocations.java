package com.sokowatch.accountinginterface.model.collection;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import com.intuit.ipp.data.Row;
import com.intuit.ipp.data.RowTypeEnum;
import com.intuit.ipp.data.Rows;
import com.sokowatch.accountinginterface.dto.LedgerEntryDTO;
import com.sokowatch.accountinginterface.dto.ObjectToDtoFacade;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;
import com.sokowatch.accountinginterface.enums.DataVersion;
import com.sokowatch.accountinginterface.model.AccountClassLocation;
import com.sokowatch.accountinginterface.model.LedgerEntry;

public class AccountClassLocations extends HashSet<AccountClassLocation> {

	private static final long serialVersionUID = 1L;

	public AccountClassLocations() {
		super();
	}

	public AccountClassLocation getAccountClassLocation(AccountClassLocation accountClassLocation) {
		Iterator<AccountClassLocation> accountClassLocationIterator = this.iterator();
		while(accountClassLocationIterator.hasNext()) {
			AccountClassLocation savedAccountClassLocation = accountClassLocationIterator.next();
			if(savedAccountClassLocation.equals(accountClassLocation)) {
				return savedAccountClassLocation;

			}			
		}

		return null;
	}

	//TODO test this
	public void populateFromReportRows(Rows rows1, AccountingPackageAccounts accountingPackageAccounts,
			String companyName, Currency companyCurrency) throws Exception {

		Iterator<Row> rowsIterator1 = rows1.getRow().iterator();

		// populate the LedgerLines, which has the detail entries per Account per month, per Location, per class.
		while (rowsIterator1.hasNext()) {
			Rows rows2 = rowsIterator1.next().getRows(); // this takes it down 1 rung. First rung is just rowsIterator.next(). By calling getRows it goes down the next level
			Iterator<Row> rowsIterator2 = rows2.getRow().iterator();
			while (rowsIterator2.hasNext()) {
				Row row2 = rowsIterator2.next();

				if(isADataRowForInclusion(row2)) {
					this.addFromRow(accountingPackageAccounts, companyName, companyCurrency, row2);
				}

				if (isASectionRow(row2)) { // 1/4 sub-account layer (max 4 allowed in QB)
					Rows rows3 = row2.getRows();

					Iterator<Row> rowsIterator3 = rows3.getRow().iterator();
					while (rowsIterator3.hasNext()) {
						Row row3 = rowsIterator3.next();
						if (isADataRowForInclusion(row3)) {
							this.addFromRow(accountingPackageAccounts, companyName, companyCurrency, row3);
						}

						if (isASectionRow(row3)) { // 2/4 sub-account layer (max 4 allowed in QB)
							Rows rows4 = row3.getRows();

							Iterator<Row> rowsIterator4 = rows4.getRow().iterator();
							while (rowsIterator4.hasNext()) {
								Row row4 = rowsIterator4.next();
								if (isADataRowForInclusion(row4)) {
									this.addFromRow(accountingPackageAccounts, companyName, companyCurrency, row4);
								}
								if (isASectionRow(row4)) { // 3/4 sub-account layer (max 4 allowed in QB)
									Rows rows5 = row4.getRows();

									Iterator<Row> rowsIterator5 = rows5.getRow().iterator();
									while (rowsIterator5.hasNext()) {
										Row row5 = rowsIterator5.next();
										if (isADataRowForInclusion(row5)) {
											this.addFromRow(accountingPackageAccounts, companyName, companyCurrency, row5);
										}
										if (isASectionRow(row5)) { // 4/4 sub-account layer (max 4 allowed in QB)
											Rows rows6 = row5.getRows();

											Iterator<Row> rowsIterator6 = rows6.getRow().iterator();
											while (rowsIterator6.hasNext()) {
												Row row6 = rowsIterator6.next();
												if (isADataRowForInclusion(row6)) {
													this.addFromRow(accountingPackageAccounts, companyName, companyCurrency, row6);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	//Note, this method is needed (rather than just using the normal add() method because we want to add the ledgerEntries to the existing ledgerEntries if the accountClassLocation already has ledgerEntries, rather than overwriting them
	public void addOrUpdateAccountClassLocation(AccountClassLocation acl) {
		if(this.contains(acl)) {
			AccountClassLocation existingAccountClassLocation = this.getAccountClassLocation(acl);
			existingAccountClassLocation.getLedgerEntries().addAll(acl.getLedgerEntries());
		}
		else {
			this.add(acl);
		}
	}
	
	public List<SummaryGLLineDTO> toSummaryGLLineDTOsForPeriod(List<LocalDate> yearMonthsCoveredByRun, LocalDate outputFromDate) {
		LocalDateTime dataRunTimeStamp = LocalDateTime.now();
		
		List<SummaryGLLineDTO> dtos = new ArrayList<SummaryGLLineDTO>();
		Iterator<LocalDate> dateIterator2 = yearMonthsCoveredByRun.iterator();
		while(dateIterator2.hasNext()) {
			Iterator<AccountClassLocation> accountClassLocationIterator = this.iterator();
			LocalDate dt2 = dateIterator2.next();
			if(dt2.compareTo(outputFromDate) >= 0) {
				while(accountClassLocationIterator.hasNext()) {
					AccountClassLocation accountClassLocation = accountClassLocationIterator.next();
					if(accountClassLocation.getClosingBalanceForMonthYear(dt2).compareTo(BigDecimal.ZERO) != 0 
							|| accountClassLocation.getTotalAmountForMonthYear(dt2).compareTo(BigDecimal.ZERO) != 0) {
						dtos.add(ObjectToDtoFacade.convertToDto(DataVersion.ACTUAL, dataRunTimeStamp, dt2, accountClassLocation));
					}
				}
			}
		}
		
		return dtos;
	}
	
	public List<LedgerEntryDTO> toSuspectedErrorJournalDTOsForPeriod(List<LocalDate> yearMonthsCoveredByRun) {
		List<LedgerEntryDTO> ledgerEntryDTOs = new ArrayList<LedgerEntryDTO>();
		
		Iterator<AccountClassLocation> accountClassLocationIterator = this.iterator();
		while(accountClassLocationIterator.hasNext()) {
			AccountClassLocation accountClassLocation = accountClassLocationIterator.next();
			Iterator<LedgerEntry> ledgerEntryIterator = accountClassLocation.getLedgerEntries().iterator();
			while(ledgerEntryIterator.hasNext()) {
				LedgerEntry ledgerEntry = ledgerEntryIterator.next();
				if(ledgerEntry.hasSuspectedError()) {
					ledgerEntryDTOs.add(ObjectToDtoFacade.convertToDto(ledgerEntry));
				}
				/* to output all journals uncomment the below few lines
				 else {
					if(ledgerEntry.getDate().isAfter(LocalDate.of(2020, 8, 31))) {
						ledgerEntryDTOs.add(ObjectToDtoFacade.convertToDto(ledgerEntry));
					}
				
				}
				*/
			}
		}
		return ledgerEntryDTOs;
	}

	private void addFromRow(AccountingPackageAccounts accountingPackageAccounts, String companyName, Currency companyCurrency,
			Row row) {
		//If you update the query and get back different columns for ledgerEntries from QB, remember you also need to update the method below called isADataRowForInclusion()
		LocalDate dt = LocalDate.parse(row.getColData().get(0).getValue(), DateTimeFormatter.ISO_LOCAL_DATE);
		String locationTag;// = row.getColData().get(1).getValue();
		String classTag = "";// = row.getColData().get(2).getValue();
		String actNum = row.getColData().get(4).getValue();
		
		//formatting account number from ledger to match with accounts
		actNum = formatToString(Double.parseDouble(actNum));

		BigDecimal amt = BigDecimal.ZERO;
		if(!row.getColData().get(5).getValue().isEmpty()) {
			amt = new BigDecimal(row.getColData().get(5).getValue());
		}
		
		//String apPaidStatus = row.getColData().get(13).getValue();
		//String arPaidStatus = row.getColData().get(12).getValue();
		//String clearedStatus = row.getColData().get(14).getValue();
		//String customerName = row.getColData().get(4).getValue();
		//String docNumber = row.getColData().get(2).getValue();
		//String memo = row.getColData().get(9).getValue();
		//String name = row.getColData().get(3).getValue();
		//String productOrServiceName = row.getColData().get(1).getValue();
		//String splitAcc = row.getColData().get(5).getValue();
		//String supplierName = row.getColData().get(5).getValue();
		String transactionType = row.getColData().get(1).getValue();
		
		if(dt.isAfter(LocalDate.of(2020, 8, 31))) { //this is post the change of tagging so handle it normally
			locationTag = row.getColData().get(2).getValue();
			classTag = row.getColData().get(3).getValue();
		}
		else { //this is the historical stuff
			if(row.getColData().get(1).getValue().equals("Phone financing")) { //then even though it is historical, it has been booked correctly, so handle it normally
				locationTag = row.getColData().get(2).getValue();
				classTag = row.getColData().get(3).getValue();
			}
			else { //this is historical stuff that needs us to shift what is currently on the class onto the location and leave the class blank
				locationTag = row.getColData().get(3).getValue(); //swapping the Class tag onto the Location Tag for historical dates and leaving Class empty
			}
		}

		AccountClassLocation accountClassLocation = new AccountClassLocation();
		if(accountingPackageAccounts.getAccountingPackageAccountWithNum(actNum)==null)
		{
			actNum = row.getColData().get(4).getValue();
		}
		accountClassLocation.setAccountingPackageAccount(accountingPackageAccounts.getAccountingPackageAccountWithNum(actNum));
		accountClassLocation.setCompanyName(companyName);
		accountClassLocation.setCompanyCurrency(companyCurrency);
		accountClassLocation.setClassTag(classTag);
		accountClassLocation.setAccountingLocation(locationTag);

		LedgerEntry ledgerEntry = new LedgerEntry();
		ledgerEntry.setDate(dt);
		ledgerEntry.setAmount(amt);
		//ledgerEntry.setApPaidStatus(apPaidStatus);
		//ledgerEntry.setArPaidStatus(arPaidStatus);
		//ledgerEntry.setClearedStatus(clearedStatus);
		//ledgerEntry.setCustomerName(customerName);
		//ledgerEntry.setDocNumber(docNumber);
		//ledgerEntry.setMemo(memo);
		//ledgerEntry.setName(name);
		//ledgerEntry.setProductOrServiceName(productOrServiceName);
		//ledgerEntry.setSplitAccount(splitAcc);
		//ledgerEntry.setSupplierName(supplierName);
		ledgerEntry.setTransactionType(transactionType);
		
		if (this.contains(accountClassLocation)) {
			ledgerEntry.setAccountClassLocation(this.getAccountClassLocation(accountClassLocation));
		} else {
			ledgerEntry.setAccountClassLocation(accountClassLocation);
			this.add(accountClassLocation);
		}
	}

	private Boolean isADataRowForInclusion(Row row) throws Exception {
		if (!row.getColData().isEmpty() && row.getColData().get(0).getValue().startsWith("Unable to display more data")) {
			throw new Exception("Run failed.  QB Max limit reached.");
		}
		
		Boolean returnVal = false;
		// check that it is a data row, that columnn zero does not contain opening
		// balance, that account no is not empty and amount has a value
		if (row.getType() == RowTypeEnum.DATA && !row.getColData().get(0).getValue().equals("Beginning Balance")
				&& !row.getColData().get(4).getValue().isEmpty() && !row.getColData().get(5).getValue().isEmpty()) { //also check account number and amount aren't empty
			returnVal = true;
		}
		return returnVal;
	}

	private Boolean isASectionRow(Row row) {
		Boolean returnVal = false;
		if (row.getType() == RowTypeEnum.SECTION) {
			returnVal = true;
		}
		return returnVal;
	}

	private static String formatToString(double d)
	{
		if(d == (long) d)
			return String.format("%d",(long)d);
		else
			return String.format("%s",d);
	}
}
