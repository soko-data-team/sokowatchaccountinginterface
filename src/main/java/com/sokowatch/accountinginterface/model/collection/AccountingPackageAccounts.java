package com.sokowatch.accountinginterface.model.collection;

import java.util.HashSet;
import java.util.Iterator;
import com.sokowatch.accountinginterface.model.AccountingPackageAccount;

public class AccountingPackageAccounts extends HashSet<AccountingPackageAccount> {
	private static final long serialVersionUID = 2L;
	
	public AccountingPackageAccounts() {
		super();
	}
	
	public AccountingPackageAccount getAccountingPackageAccountWithNum(String accountNum) {
		Iterator<AccountingPackageAccount> accountingPackageAccountIterator = this.iterator();
		while(accountingPackageAccountIterator.hasNext()) {
			AccountingPackageAccount savedAccount = accountingPackageAccountIterator.next();
			if(savedAccount.getNum() != null) {
				if(savedAccount.getNum().equals(accountNum)) {
					return savedAccount;
				}
			}
		}
		return null;
	}
}
