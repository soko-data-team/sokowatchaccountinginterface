package com.sokowatch.accountinginterface.encrypt;

import static org.apache.commons.codec.binary.Hex.*;
import static org.apache.commons.io.FileUtils.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.sokowatch.accountinginterface.service.config.ConfigServiceImpl;

@Component
public class SecretKeyHelper {
	//@Autowired
	//private org.springframework.core.env.Environment env;
	@Value( "${ConfigFileLocation}" )
	private String configFileLocation;
	
	public SecretKeyHelper() {
	}
	
	public SecretKey generateKey() throws NoSuchAlgorithmException
	{
	    KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
	    keyGenerator.init(256); // 128 default; 192 and 256 also possible
	    return keyGenerator.generateKey();
	}
	
	public Boolean keyExists() {
		File file = new File(configFileLocation + "98drtWxD.no");
		
		Boolean returnVal = false;
		if(file.exists()) {
			returnVal = true;
		}
		return returnVal;
	}

	public void saveKey(SecretKey key) throws IOException
	{
	    File file = new File(configFileLocation + "98drtWxD.no");
		char[] hex = encodeHex(key.getEncoded());
	    writeStringToFile(file, String.valueOf(hex), "ISO-8859-1");
	}

	public SecretKey getKey() throws IOException
	{
		File file = new File(configFileLocation + "98drtWxD.no");
	    String data = new String(readFileToByteArray(file));
	    byte[] encoded;
	    try {
	        encoded = decodeHex(data.toCharArray());
	    } catch (DecoderException e) {
	        e.printStackTrace();
	        return null;
	    }
	    return new SecretKeySpec(encoded, "AES");
	}
}
