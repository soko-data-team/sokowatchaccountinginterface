package com.sokowatch.accountinginterface.service.token;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sokowatch.accountinginterface.repository.AccessTokenRepository;

@Service
public class AccessTokenServiceImpl implements TokenService {
	@Autowired AccessTokenRepository repository;
	private static final Logger logger = Logger.getLogger(AccessTokenServiceImpl.class);
	
	@Override
	public String getTokenForRealmId(String realmId) throws Exception {
		return repository.findTokenForRealmId(realmId);
	}
	
	@Override
	public void createTokenForRealmId(String realmId, String token) throws Exception {
		repository.saveTokenForRealmId(realmId, token);
	}
}
