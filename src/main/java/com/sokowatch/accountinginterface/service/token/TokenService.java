package com.sokowatch.accountinginterface.service.token;

import java.io.IOException;

public interface TokenService {
	String getTokenForRealmId(String realmId) throws IOException, Exception;
	void createTokenForRealmId(String realmId, String token) throws IOException, Exception;
}