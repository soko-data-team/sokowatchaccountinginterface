package com.sokowatch.accountinginterface.service.token;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sokowatch.accountinginterface.repository.RefreshTokenRepository;

@Service
public class RefreshTokenServiceImpl implements TokenService {
	@Autowired RefreshTokenRepository repository;
	private static final Logger logger = Logger.getLogger(RefreshTokenServiceImpl.class);
	
	@Override
	public String getTokenForRealmId(String realmId) throws IOException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
		return repository.findTokenForRealmId(realmId);
	}
		
	@Override
	public void createTokenForRealmId(String realmId, String token) throws IOException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		repository.saveTokenForRealmId(realmId, token);
	}
}
