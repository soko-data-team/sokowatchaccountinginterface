package com.sokowatch.accountinginterface.service.email;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
	private static final Logger logger = Logger.getLogger(EmailServiceImpl.class);
	private final String emailFrom = "noreply@sokowatch.com";
	@Autowired
    private JavaMailSender emailSender;
 
	@Override
	public void sendSimpleMessage(String to, String subject, String text) {
    	logger.debug("Sending Simple Email");
    	SimpleMailMessage message = new SimpleMailMessage(); 
        message.setFrom(emailFrom);
        message.setTo(to); 
        message.setSubject(subject); 
        message.setText(text);
        emailSender.send(message);
        logger.debug("Simple Email Sent");
    }
	
	@Override
	public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) throws MessagingException {
		logger.debug("Sending Attachment Email");
		MimeMessage message = emailSender.createMimeMessage();
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setFrom(emailFrom);
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setText(text);
	        
	    FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
	    helper.addAttachment("SuspectedErrors.csv", file);
	 
	    emailSender.send(message);
	    logger.debug("Sent Attachment Email");
	}
}
