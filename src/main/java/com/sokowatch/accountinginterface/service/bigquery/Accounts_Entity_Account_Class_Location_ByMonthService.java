package com.sokowatch.accountinginterface.service.bigquery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.google.cloud.bigquery.TableResult;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;

public interface Accounts_Entity_Account_Class_Location_ByMonthService {

	TableResult getAll() throws InterruptedException, FileNotFoundException, IOException;

	void uploadData(List<SummaryGLLineDTO> dtos) throws FileNotFoundException, IOException, Exception;

}