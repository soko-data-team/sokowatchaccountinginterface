package com.sokowatch.accountinginterface.service.bigquery;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.cloud.bigquery.BigQueryException;
import com.google.cloud.bigquery.TableResult;
import com.sokowatch.accountinginterface.dto.SummaryGLLineDTO;
import com.sokowatch.accountinginterface.repository.bigquery.Accounts_Entity_Account_Class_Location_ByMonthRepository;

@Service
public class Accounts_Entity_Account_Class_Location_ByMonthServiceImpl implements Accounts_Entity_Account_Class_Location_ByMonthService {
	@Autowired
	public Accounts_Entity_Account_Class_Location_ByMonthRepository repository;
	private static final Logger logger = Logger.getLogger(Accounts_Entity_Account_Class_Location_ByMonthServiceImpl.class);
	
	@Override
	public TableResult getAll() throws InterruptedException, FileNotFoundException, IOException {
		logger.debug("Getting all data from BigQuery");
		TableResult tableResult = repository.findAll();
		return tableResult;
	}
	
	@Override
	public void uploadData(List<SummaryGLLineDTO> dtos) throws Exception {
		repository.runTableInsertRowsWithoutRowIds(dtos);
	}
}
