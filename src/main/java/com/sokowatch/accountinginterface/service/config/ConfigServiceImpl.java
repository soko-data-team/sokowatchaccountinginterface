package com.sokowatch.accountinginterface.service.config;

import java.io.IOException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sokowatch.accountinginterface.repository.ConfigRepository;

@Service
public class ConfigServiceImpl {
	private static final Logger logger = Logger.getLogger(ConfigServiceImpl.class);
	@Autowired ConfigRepository repository;
	
	public String getConfigValue(String configValue) throws IOException {
		return repository.findConfigValue(configValue);
	}
}
