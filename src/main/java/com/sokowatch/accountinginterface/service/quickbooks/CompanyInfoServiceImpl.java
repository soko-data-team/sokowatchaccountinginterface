package com.sokowatch.accountinginterface.service.quickbooks;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.intuit.ipp.data.CompanyInfo;
import com.intuit.ipp.data.Preferences;
import com.intuit.ipp.exception.FMSException;
import com.sokowatch.accountinginterface.repository.quickbooks.CompanyInfoRepository;
import com.sokowatch.accountinginterface.repository.quickbooks.CompanyPreferencesRepository;

@Service
public class CompanyInfoServiceImpl implements CompanyInfoService {
	
	@Autowired
	public CompanyPreferencesRepository companyPreferencesRepository;
	@Autowired
	public CompanyInfoRepository companyInfoRepository;

	//private static final Logger logger = Logger.getLogger(GeneralLedgerServiceImpl.class);
	
	@Override
	public Preferences getCompanyPreferences(String realmId, String accessToken) throws FMSException, IOException {
		return companyPreferencesRepository.findCompanyPreferences(realmId, accessToken);
	}
		
	@Override
	public CompanyInfo getCompanyInfo(String realmId, String accessToken) throws FMSException, IOException {
		return companyInfoRepository.findCompanyInfo(realmId, accessToken);
	}

}
