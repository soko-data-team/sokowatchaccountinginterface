package com.sokowatch.accountinginterface.service.quickbooks;

import com.sokowatch.accountinginterface.model.collection.AccountingPackageAccounts;

public interface AccountingPackageAccountsService {
	AccountingPackageAccounts getAccountingPackageAccounts(String realmId, String accessToken) throws Exception;
}