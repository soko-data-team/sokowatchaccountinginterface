package com.sokowatch.accountinginterface.service.quickbooks;

import java.io.IOException;
import java.time.LocalDate;

import com.intuit.ipp.data.Report;
import com.intuit.ipp.exception.FMSException;
import com.sokowatch.accountinginterface.model.collection.AccountClassLocations;

public interface GeneralLedgerService {
	Report getLedgerReportForTimePeriod(String realmId, String accessToken, LocalDate fromDate, LocalDate toDate)
			throws FMSException, IOException;

	AccountClassLocations getAccountClassLocationsForYearMonths(String realmId, String accessToken, LocalDate fromDate,
			LocalDate toDate) throws Exception;
}