package com.sokowatch.accountinginterface.service.quickbooks;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Currency;
import java.util.Iterator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.intuit.ipp.data.Preferences;
import com.intuit.ipp.data.Report;
import com.intuit.ipp.data.Rows;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.services.ReportName;
import com.intuit.ipp.services.ReportService;
import com.sokowatch.accountinginterface.helper.QBOServiceHelper;
import com.sokowatch.accountinginterface.model.AccountClassLocation;
import com.sokowatch.accountinginterface.model.collection.AccountClassLocations;
import com.sokowatch.accountinginterface.model.collection.AccountingPackageAccounts;
import com.sokowatch.accountinginterface.repository.quickbooks.AccountingReportRepository;
import com.sokowatch.accountinginterface.utilities.DateHelper;

@Service
public class GeneralLedgerServiceImpl implements GeneralLedgerService {
	@Autowired
	AccountingReportRepository accountingReportRepository;
	@Autowired
	AccountingPackageAccountsService accountingPackageAccountsService;
	@Autowired
	CompanyInfoService companyInfoService;
	@Autowired
	QBOServiceHelper helper;

	private static final Logger logger = Logger.getLogger(GeneralLedgerServiceImpl.class);
	
	//TODO this is for testing so can remove once done testing
	@Override
	public Report getLedgerReportForTimePeriod(String realmId, String accessToken,
			LocalDate fromDate, LocalDate toDate) throws FMSException, IOException {

		ReportService service;
		Report ledger = new Report();
		//try {
		service = helper.getReportService(realmId, accessToken);
		service.setStart_date(fromDate.toString());
		service.setEnd_date(toDate.toString());

		//service.setColumns("tx_date,account_num,subt_nat_amount,klass_name,dept_name");
		ledger = service.executeReport(ReportName.GENERALLEDGER.toString());
		/*} catch (FMSException e) {
			e.printStackTrace();
		} */

		return ledger;
	}
	
	@Override
	public AccountClassLocations getAccountClassLocationsForYearMonths(String realmId, String accessToken,
			LocalDate fromDate, LocalDate toDate) throws Exception {
		
		//TODO This badly needs an integration test, but the problem is it requires a login to QB's
		
		String companyName = companyInfoService.getCompanyInfo(realmId, accessToken).getCompanyName();
		Preferences companyPreferences = companyInfoService.getCompanyPreferences(realmId, accessToken);
		Currency companyCurrency = Currency.getInstance(companyPreferences.getCurrencyPrefs().getHomeCurrency().getValue());
		Boolean companyIsMultiCurrency = companyPreferences.getCurrencyPrefs().isMultiCurrencyEnabled();
		// get all the accounts
		AccountingPackageAccounts accountingPackageAccounts = accountingPackageAccountsService.getAccountingPackageAccounts(realmId, accessToken);
		
		AccountClassLocations accountClassLocationsForWholeTimePeriod = new AccountClassLocations();
		
		/*I break this up into a [x] monthly call because QB's has a limit on the amount of data one can call with a single call.
		This slows it down, but ensures I get the data
		*/
		// Long maxMonthGap = 0L;
		Long maxDayGap = 10L;
		//Set loopDateEnd for first loop, which is a max of the (fromDate + gap) or the toDate
		// LocalDate loopDateEnd = DateHelper.getConstrainedDate(fromDate.plusMonths(maxMonthGap).with(TemporalAdjusters.lastDayOfMonth()), toDate);
		LocalDate loopDateEnd = DateHelper.getConstrainedDate(fromDate.plusDays(maxDayGap), toDate);
		LocalDate loopDateStart = LocalDate.of(fromDate.getYear(), fromDate.getMonth(), fromDate.getDayOfMonth());
		while(loopDateEnd.compareTo(toDate)<=0) {
			
			AccountClassLocations accountClassLocationsForPeriod = this.getAccountClassLocationsForPeriod(realmId, 
					accessToken, loopDateStart, loopDateEnd,
					companyName,companyCurrency, companyIsMultiCurrency, accountingPackageAccounts);
			
			Iterator<AccountClassLocation> accountClassLocationForPeriodIterator = accountClassLocationsForPeriod.iterator();
			while(accountClassLocationForPeriodIterator.hasNext()) {
				accountClassLocationsForWholeTimePeriod.addOrUpdateAccountClassLocation(accountClassLocationForPeriodIterator.next());
			}
			
			loopDateStart = loopDateEnd.plusDays(1L);//moves loopDateStart to the 1st day of the next month group
			// loopDateEnd = DateHelper.getConstrainedDate(loopDateStart.plusMonths(maxMonthGap).with(TemporalAdjusters.lastDayOfMonth()),toDate);
			loopDateEnd = DateHelper.getConstrainedDate(loopDateStart.plusDays(maxDayGap),toDate);

			if(loopDateStart.compareTo(loopDateEnd) > 0) { //if the loopDateEnd has been constrained to a max of the toDate, then we have run through all months
				break;
			}
		}
			
		return accountClassLocationsForWholeTimePeriod;
	}
	
	private AccountClassLocations getAccountClassLocationsForPeriod(String realmId, String accessToken,
			LocalDate fromDate, LocalDate toDate, 
			String companyName, Currency companyCurrency, Boolean companyIsMultiCurrency,
			AccountingPackageAccounts accountingPackageAccounts) throws Exception {

		logger.debug("Start getting Ledger Data for the period " + fromDate.toString() + " to " + toDate.toString());
		Report ledgerReport = accountingReportRepository.findLedgerReportForTimePeriod(realmId, accessToken, fromDate, toDate, companyIsMultiCurrency);
		logger.debug("Finished Getting Ledger Data for the period");
		
		AccountClassLocations accountClassLocations = new AccountClassLocations();
		Rows rows1 = ledgerReport.getRows(); // this gets the first layer of rows
		accountClassLocations.populateFromReportRows(rows1, accountingPackageAccounts, companyName, companyCurrency);
		
		return accountClassLocations;
	}
}
