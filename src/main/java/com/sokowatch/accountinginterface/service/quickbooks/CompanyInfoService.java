package com.sokowatch.accountinginterface.service.quickbooks;

import java.io.IOException;

import com.intuit.ipp.data.CompanyInfo;
import com.intuit.ipp.data.Preferences;
import com.intuit.ipp.exception.FMSException;

public interface CompanyInfoService {
	Preferences getCompanyPreferences(String realmId, String accessToken) throws FMSException, IOException;
	CompanyInfo getCompanyInfo(String realmId, String accessToken) throws FMSException, IOException;
}