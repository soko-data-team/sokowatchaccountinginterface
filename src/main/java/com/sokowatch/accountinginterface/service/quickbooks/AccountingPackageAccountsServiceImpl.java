package com.sokowatch.accountinginterface.service.quickbooks;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sokowatch.accountinginterface.model.collection.AccountingPackageAccounts;
import com.sokowatch.accountinginterface.repository.quickbooks.AccountingPackageAccountsRepository;

@Service
public class AccountingPackageAccountsServiceImpl implements AccountingPackageAccountsService {
	@Autowired
	public AccountingPackageAccountsRepository accountingPackageAccountsRepository;

	private static final Logger logger = Logger.getLogger(AccountingPackageAccountsServiceImpl.class);
	
	@Override
	public AccountingPackageAccounts getAccountingPackageAccounts(String realmId, String accessToken) throws Exception {
		logger.debug("Getting all accounts from accounting system");
		return accountingPackageAccountsRepository.findAccountingPackageAccounts(realmId, accessToken);
	}
}
